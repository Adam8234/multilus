package com.adam8234.multilus;

import com.adam8234.multilus.blocks.BlockSpecialStone;
import com.adam8234.multilus.common.IProxy;
import com.adam8234.multilus.common.MLContent;
import com.adam8234.multilus.common.MLRepo;
import com.adam8234.multilus.generator.WorldGeneratorManager;
import com.adam8234.multilus.handler.GuiHandler;
import com.adam8234.multilus.handler.MultiLusEventHandler;
import com.adam8234.multilus.network.PacketHandler;
import com.adam8234.multilus.recipe.RecipeHandler;
import com.adam8234.multilus.reference.EnumStained;
import com.adam8234.multilus.reference.Reference;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.EnumHelper;

@Mod(modid = Reference.MOD_ID, acceptedMinecraftVersions = "1.7", dependencies = "required-after:CodeChickenCore")
public class MultiLus {
    public static final CreativeTabs tabBlock = new CreativeTabs(CreativeTabs.getNextID(), Reference.MOD_ID.toLowerCase() + ".tabBlocks") {
        @Override
        public ItemStack getIconItemStack() {
            return BlockSpecialStone.EnumSpecialStone.DARK_STONE.getItemStack();
        }

        @Override
        public Item getTabIconItem() {
            return getIconItemStack().getItem();
        }

    };
    public static final CreativeTabs tabItem = new CreativeTabs(CreativeTabs.getNextID(), Reference.MOD_ID.toLowerCase() + ".tabItems") {
        @Override
        public ItemStack getIconItemStack() {
            return EnumStained.BLACK.getCrystal();
        }

        @Override
        public Item getTabIconItem() {
            return getIconItemStack().getItem();
        }
    };

    @Mod.Instance(Reference.MOD_ID)
    public static MultiLus instance;

    public static MLContent content;

    @SidedProxy(clientSide = "com.adam8234.multilus.client.ClientProxy", serverSide = "com.adam8234.multilus.common.ServerProxy")
    public static IProxy proxy;

    public static final Item.ToolMaterial lusToolMaterial = EnumHelper.addToolMaterial("lus", 2, 512, 6.5F, 2, 14);

    public MultiLus() {
        MinecraftForge.EVENT_BUS.register(new MultiLusEventHandler());
    }

    @EventHandler
    public void init(FMLInitializationEvent event) {
        NetworkRegistry.INSTANCE.registerGuiHandler(instance, new GuiHandler());
        proxy.init();
        content.registerMicroMaterials();
        content.registerTileEntites();
        new RecipeHandler();
        GameRegistry.registerWorldGenerator(WorldGeneratorManager.instance, 3);
    }

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        PacketHandler.init();
        content = new MLContent();
        proxy.preInit();
    }

}
