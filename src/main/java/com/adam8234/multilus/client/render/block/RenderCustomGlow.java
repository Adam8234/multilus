package com.adam8234.multilus.client.render.block;

import codechicken.lib.render.CCModel;
import codechicken.lib.render.CCRenderState;
import codechicken.lib.render.uv.IconTransformation;
import codechicken.lib.vec.Cuboid6;
import codechicken.lib.vec.Translation;
import codechicken.lib.vec.Vector3;
import com.adam8234.multilus.blocks.lus.BlockLus;
import com.adam8234.multilus.blocks.lus.BlockLusGlass;
import com.adam8234.multilus.blocks.lus.IBlockLus;
import com.adam8234.multilus.blocks.lus.custom.BlockLusGlassCustom;
import com.adam8234.multilus.blocks.lus.custom.IBlockLusCustom;
import com.adam8234.multilus.client.render.MLRenderHelper;
import com.adam8234.multilus.common.MLRepo;
import com.adam8234.multilus.reference.MLColors;
import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import cpw.mods.fml.client.registry.RenderingRegistry;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.world.IBlockAccess;

public class RenderCustomGlow extends RenderBlock implements ISimpleBlockRenderingHandler {

    public static final int modelId = RenderingRegistry.getNextAvailableRenderId();
    private static final CCModel baseModel = CCModel.quadModel(24).generateBlock(0, new Cuboid6(new Vector3(0.01, 0.01, 0.01), new Vector3(.99, .99, .99))).apply(new Translation(new Vector3(-.5, -.5, -.5)));
    private static final CCModel overlayModel = CCModel.quadModel(24).generateBlock(0, Cuboid6.full).apply(new Translation(new Vector3(-.5, -.5, -.5)));


    @Override
    public int getRenderId() {
        return modelId;
    }

    @Override
    public void renderInventoryBlock(Block block, int metadata, int modelId, RenderBlocks renderer) {
        CCRenderState.reset();
        CCRenderState.useNormals = true;
        CCRenderState.startDrawing();
        CCRenderState.setBrightness(((IBlockLus) block).getBrightness());
        //Casting has to be done for glass to render properly.
        baseModel.computeNormals().render(new IconTransformation(block instanceof BlockLusGlassCustom ? BlockLus.glassTexture : BlockLus.baseTexture));
        CCRenderState.draw();
        renderStandardInvBlock(renderer, block, metadata);
    }

    @Override
    public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer) {
        ((IBlockLusCustom) block).getRGBColor(world, x, y, z, Tessellator.instance);
        Tessellator.instance.setBrightness(15 << 20 | 15 << 4);
        if (block instanceof BlockLusGlassCustom)
            renderer.setRenderBounds(0.00001, 0.00001, 0.00001, .99999, .99999, .99999);
        renderAllSides(world, x, y, z, block, renderer, block instanceof BlockLusGlassCustom ? BlockLus.glassTexture : BlockLus.baseTexture, false);
        renderer.setRenderBoundsFromBlock(block);
        renderer.renderStandardBlock(block, x, y, z);
        return true;
    }

    @Override
    public boolean shouldRender3DInInventory(int i) {
        return true;
    }
}
