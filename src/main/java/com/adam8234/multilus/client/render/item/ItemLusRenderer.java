package com.adam8234.multilus.client.render.item;

import com.adam8234.multilus.client.render.MLRenderHelper;
import com.adam8234.multilus.item.lus.IItemLus;
import com.adam8234.multilus.item.lus.ItemLus;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraftforge.client.IItemRenderer;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class ItemLusRenderer implements IItemRenderer {
    /**
     * Checks if this renderer should handle a specific item's render type
     *
     * @param item The item we are trying to render
     * @param type A render type to check if this renderer handles
     * @return true if this renderer should handle the given render type,
     * otherwise false
     */
    @Override
    public boolean handleRenderType(ItemStack item, ItemRenderType type) {
        return (type.ordinal() < ItemRenderType.FIRST_PERSON_MAP.ordinal());
    }

    /**
     * Checks if certain helper functionality should be executed for this renderer.
     * See ItemRendererHelper for more info
     *
     * @param type   The render type
     * @param item   The ItemStack being rendered
     * @param helper The type of helper functionality to be ran
     * @return True to run the helper functionality, false to not.
     */
    @Override
    public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
        return handleRenderType(item, type) & helper.ordinal() < ItemRendererHelper.EQUIPPED_BLOCK.ordinal();
    }

    /**
     * Called to do the actual rendering, see ItemRenderType for details on when specific
     * types are run, and what extra data is passed into the data parameter.
     *
     * @param type The render type
     * @param item The ItemStack being rendered
     * @param data Extra Type specific data
     */
    @Override
    public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
        Tessellator tessellator = Tessellator.instance;

        IIcon icon = item.getItem().getIcon(item, 0);
        IIcon mask = ((IItemLus) item.getItem()).getMaskIcon();
        IIcon animationIcon = ItemLus.animationIcon;

        float iconMinX = icon.getMinU();
        float iconMaxX = icon.getMaxU();
        float iconMinY = icon.getMinV();
        float iconMaxY = icon.getMaxV();

        float maskMinX = mask.getMinU();
        float maskMaxX = mask.getMaxU();
        float maskMinY = mask.getMinV();
        float maskMaxY = mask.getMaxV();

        float animationMinX = animationIcon.getMinU();
        float animationMaxX = animationIcon.getMaxU();
        float animationMinY = animationIcon.getMinV();
        float animationMaxY = animationIcon.getMaxV();

        GL11.glPushMatrix();

        switch (type) {
            case EQUIPPED_FIRST_PERSON:
                GL11.glTranslated(1.0D, 1.0D, 0.0D);
                GL11.glRotated(180.0D, 0.0D, 0.0D, 1.0D);
                break;
        }

        TextureUtil.func_152777_a(false, false, 1.0f);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        OpenGlHelper.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA, GL11.GL_ONE, GL11.GL_ZERO);

        if (type == ItemRenderType.INVENTORY) {
            GL11.glDisable(GL11.GL_LIGHTING);

            //Render Icon
            tessellator.startDrawingQuads();
            tessellator.addVertexWithUV(0, 16, 0, iconMinX, iconMaxY);
            tessellator.addVertexWithUV(16, 16, 0, iconMaxX, iconMaxY);
            tessellator.addVertexWithUV(16, 0, 0, iconMaxX, iconMinY);
            tessellator.addVertexWithUV(0, 0, 0, iconMinX, iconMinY);
            tessellator.draw();

            //Render Mask
            tessellator.startDrawingQuads();
            tessellator.addVertexWithUV(0, 16, 0.001, maskMinX, maskMaxY);
            tessellator.addVertexWithUV(16, 16, 0.001, maskMaxX, maskMaxY);
            tessellator.addVertexWithUV(16, 0, 0.001, maskMaxX, maskMinY);
            tessellator.addVertexWithUV(0, 0, 0.001, maskMinX, maskMinY);
            tessellator.draw();

            GL11.glEnable(GL11.GL_CULL_FACE);
            GL11.glDepthFunc(GL11.GL_EQUAL);
            GL11.glDepthMask(false);
            GL11.glMatrixMode(GL11.GL_TEXTURE);
            OpenGlHelper.glBlendFunc(GL11.GL_ONE, GL11.GL_ZERO, GL11.GL_ONE, GL11.GL_ZERO);

            //Render animation
            tessellator.startDrawingQuads();
            tessellator.setColorOpaque_I(item.getItem().getColorFromItemStack(item, 0));
            tessellator.addVertexWithUV(0, 16, 0.001, animationMinX, animationMaxY);
            tessellator.addVertexWithUV(16, 16, 0.001, animationMaxX, animationMaxY);
            tessellator.addVertexWithUV(16, 0, 0.001, animationMaxX, animationMinY);
            tessellator.addVertexWithUV(0, 0, 0.001, animationMinX, animationMinY);
            tessellator.draw();

            GL11.glMatrixMode(GL11.GL_MODELVIEW);
            GL11.glDepthMask(true);
            GL11.glDepthFunc(GL11.GL_LEQUAL);

            GL11.glEnable(GL11.GL_LIGHTING);
        } else {
            GL11.glEnable(GL12.GL_RESCALE_NORMAL);

            if (type == ItemRenderType.ENTITY) {
                ((IItemLus) item.getItem()).getColour(item).glColour();
                GL11.glTranslatef(0.5f, 4 / -16f, 0);
                GL11.glRotatef(180, 0, 1, 0);
            }
            //Render Icon
            ItemRenderer.renderItemIn2D(tessellator, iconMaxX, iconMinY, iconMinX, iconMaxY, icon.getIconWidth(), icon.getIconHeight(), 0.0625F);

            //Render Mask
            tessellator.startDrawingQuads();
            tessellator.setNormal(0, 0, 1);
            tessellator.addVertexWithUV(0, 0, 0.001, maskMaxX, maskMaxY);
            tessellator.addVertexWithUV(1, 0, 0.001, maskMinX, maskMaxY);
            tessellator.addVertexWithUV(1, 1, 0.001, maskMinX, maskMinY);
            tessellator.addVertexWithUV(0, 1, 0.001, maskMaxX, maskMinY);
            tessellator.draw();

            //Render Mask 2
            tessellator.startDrawingQuads();
            tessellator.setNormal(0, 0, -1);
            double zPos = -0.058;
            tessellator.addVertexWithUV(0, 1, zPos, maskMinX, maskMinY);
            tessellator.addVertexWithUV(1, 1, zPos, maskMaxX, maskMinY);
            tessellator.addVertexWithUV(1, 0, zPos, maskMaxX, maskMaxY);
            tessellator.addVertexWithUV(0, 0, zPos, maskMinX, maskMaxY);
            tessellator.draw();


            GL11.glEnable(GL11.GL_CULL_FACE);
            GL11.glDepthFunc(GL11.GL_EQUAL);
            GL11.glDepthMask(false);
            OpenGlHelper.glBlendFunc(GL11.GL_ONE, GL11.GL_ZERO, GL11.GL_ONE, GL11.GL_ZERO);
            MLRenderHelper.glowOn();

            //Render animation
            tessellator.startDrawingQuads();
            tessellator.setNormal(0, 0, 1);
            tessellator.addVertexWithUV(0, 0, 0.001, animationMaxX, animationMaxY);
            tessellator.addVertexWithUV(1, 0, 0.001, animationMinX, animationMaxY);
            tessellator.addVertexWithUV(1, 1, 0.001, animationMinX, animationMinY);
            tessellator.addVertexWithUV(0, 1, 0.001, animationMaxX, animationMinY);
            tessellator.draw();

            //Render animation 2
            tessellator.startDrawingQuads();
            tessellator.setNormal(0, 0, -1);
            tessellator.addVertexWithUV(0, 1, zPos, animationMinX, animationMinY);
            tessellator.addVertexWithUV(1, 1, zPos, animationMaxX, animationMinY);
            tessellator.addVertexWithUV(1, 0, zPos, animationMaxX, animationMaxY);
            tessellator.addVertexWithUV(0, 0, zPos, animationMinX, animationMaxY);
            tessellator.draw();

            GL11.glDepthMask(true);
            GL11.glDepthFunc(GL11.GL_LEQUAL);
            GL11.glDisable(GL12.GL_RESCALE_NORMAL);
            MLRenderHelper.glowOff();
        }

        OpenGlHelper.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA, GL11.GL_ONE, GL11.GL_ZERO);
        GL11.glDisable(GL11.GL_ALPHA_TEST);
        TextureUtil.func_147945_b();
        GL11.glPopMatrix();
    }
}
