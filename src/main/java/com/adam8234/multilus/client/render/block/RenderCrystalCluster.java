package com.adam8234.multilus.client.render.block;

import codechicken.lib.render.CCModel;
import codechicken.lib.render.CCModelLibrary;
import codechicken.lib.render.CCRenderState;
import codechicken.lib.render.uv.IconTransformation;
import codechicken.lib.vec.*;
import com.adam8234.multilus.blocks.lus.BlockLus;
import com.adam8234.multilus.common.MLBlocks;
import com.adam8234.multilus.item.lus.ItemLus;
import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import cpw.mods.fml.client.registry.RenderingRegistry;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;

public class RenderCrystalCluster extends TileEntitySpecialRenderer implements ISimpleBlockRenderingHandler {

    public static final int modelId = RenderingRegistry.getNextAvailableRenderId();

    @Override
    public void renderInventoryBlock(Block block, int metadata, int modelId, RenderBlocks renderer) {
        //Render inventory
    }

    @Override
    public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer) {
        return true;
    }

    @Override
    public boolean shouldRender3DInInventory(int modelId) {
        return true;
    }

    @Override
    public int getRenderId() {
        return modelId;
    }

    @Override
    public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float partialTick) {
        CCModel model = CCModelLibrary.icosahedron7.copy().apply(new TransformationList(new Scale(.25, 1, .25), new Scale(.25)));
        CCRenderState.reset();
        CCRenderState.useNormals = true;
        CCRenderState.setBrightness(tileEntity.getWorldObj(), tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord);
        CCRenderState.changeTexture("multilus:textures/blocks/stone/blockObfuscousStone.png");
        CCRenderState.startDrawing();
        CCModel.quadModel(24).generateBlock(0, new Cuboid6(new Vector3(0, 0, 0), new Vector3(1, .25, 1))).computeNormals().render(new Translation(x, y, z));
        CCRenderState.draw();

        CCRenderState.reset();
        CCRenderState.changeTexture("multilus:textures/blocks/overlay/stainedEffect.png");
        CCRenderState.startDrawing();
        CCRenderState.useNormals = true;
        model.computeNormals().render(new Translation(x, y, z));
        CCRenderState.draw();
    }
}
