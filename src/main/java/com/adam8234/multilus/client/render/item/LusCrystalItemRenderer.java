package com.adam8234.multilus.client.render.item;

import codechicken.lib.render.CCModel;
import codechicken.lib.render.CCModelLibrary;
import codechicken.lib.render.CCRenderState;
import codechicken.lib.render.uv.IconTransformation;
import codechicken.lib.vec.*;
import com.adam8234.multilus.blocks.lus.BlockLus;
import com.adam8234.multilus.common.MLRepo;
import com.adam8234.multilus.item.lus.ItemLus;
import com.adam8234.multilus.reference.MLColors;
import com.adam8234.multilus.reference.Reference;
import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;

public class LusCrystalItemRenderer implements IItemRenderer {

    public static ResourceLocation texture = new ResourceLocation(Reference.MOD_ID.toLowerCase(), "textures/models/crystal.png");
    public static CCModel model = CCModelLibrary.icosahedron7.copy().apply(new TransformationList(new Scale(new Vector3(.35, 1, .35))));

    public LusCrystalItemRenderer() {
    }

    /**
     * Checks if this renderer should handle a specific item's render type
     *
     * @param item The item we are trying to render
     * @param type A render type to check if this renderer handles
     * @return true if this renderer should handle the given render type,
     * otherwise false
     */
    @Override
    public boolean handleRenderType(ItemStack item, ItemRenderType type) {
        return true;
    }

    /**
     * Checks if certain helper functionality should be executed for this renderer.
     * See ItemRendererHelper for more info
     *
     * @param type   The render type
     * @param item   The ItemStack being rendered
     * @param helper The type of helper functionality to be ran
     * @return True to run the helper functionality, false to not.
     */
    @Override
    public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
        return true;
    }

    /**
     * Called to do the actual rendering, see ItemRenderType for details on when specific
     * types are run, and what extra data is passed into the data parameter.
     *
     * @param type The render type
     * @param item The ItemStack being rendered
     * @param data Extra Type specific data
     */
    @Override
    public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
        Transformation transformation = null;
        CCRenderState.reset();
        CCRenderState.useNormals = true;
        CCRenderState.startDrawing();

        switch (type) {
            case INVENTORY:
                transformation = new TransformationList(new Scale(.40));
                break;
            case EQUIPPED_FIRST_PERSON:
                CCRenderState.setBrightness(15 << 20 | 15 << 4);
                transformation = new TransformationList(new Scale(.3), new Translation(new Vector3(0F, 1F, .75F)), new Rotation(270, new Vector3(1, 0, 0)));
                break;
            case ENTITY:
                CCRenderState.setBrightness(15 << 20 | 15 << 4);
                transformation = new TransformationList(new Scale(.3), new Translation(new Vector3(0F, .5F, 0F)));
                break;
            case EQUIPPED:
                CCRenderState.setBrightness(15 << 20 | 15 << 4);
                transformation = new TransformationList(new Scale(.5), new Translation(new Vector3(.7, 1, .7)));
                break;
        }

        model.setColour(MLColors.VALID_COLORS[item.getItemDamage()].c.rgba()).computeNormals().render(transformation, new IconTransformation(ItemLus.animationIcon));
        CCRenderState.draw();
    }
}
