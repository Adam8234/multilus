package com.adam8234.multilus.client.render;

import codechicken.lib.render.CCRenderState;
import net.minecraft.client.renderer.OpenGlHelper;
import org.lwjgl.opengl.GL11;

public class MLRenderHelper {
    private static float lightmapLastY = 0.0F;
    private static float lightmapLastX = 0.0F;

    public static void glowOn() {
        GL11.glPushAttrib(GL11.GL_LIGHTING_BIT);
        lightmapLastX = OpenGlHelper.lastBrightnessX;
        lightmapLastY = OpenGlHelper.lastBrightnessY;
        net.minecraft.client.renderer.RenderHelper.disableStandardItemLighting();
        OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240.0F, 240.0F);
    }

    public static void glowOff(){
        OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, lightmapLastX, lightmapLastY);
        GL11.glPopAttrib();
    }

    public static void brightnessGlow(){
        CCRenderState.setBrightness(15 << 20 | 15 << 4);
    }
}
