package com.adam8234.multilus.client.render.block;

import codechicken.lib.render.CCModel;
import codechicken.lib.render.CCRenderState;
import codechicken.lib.render.uv.IconTransformation;
import codechicken.lib.vec.Cuboid6;
import codechicken.lib.vec.Translation;
import codechicken.lib.vec.Vector3;
import com.adam8234.multilus.blocks.lus.BlockLus;
import com.adam8234.multilus.blocks.lus.BlockLusGlass;
import com.adam8234.multilus.blocks.lus.IBlockLus;
import com.adam8234.multilus.reference.MLColors;
import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import cpw.mods.fml.client.registry.RenderingRegistry;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.world.IBlockAccess;

public class RenderGlow extends RenderBlock implements ISimpleBlockRenderingHandler {
    public static final int modelId = RenderingRegistry.getNextAvailableRenderId();
    private static final CCModel baseModel = CCModel.quadModel(24).generateBlock(0, new Cuboid6(new Vector3(0.001, 0.001, 0.001), new Vector3(.999, .999, .999))).apply(new Translation(new Vector3(-.5, -.5, -.5)));

    @Override
    public int getRenderId() {
        return modelId;
    }

    @Override
    public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer) {

        CCRenderState.reset();
        CCRenderState.useNormals = true;
        CCRenderState.startDrawing();
        CCRenderState.setBrightness(((IBlockLus) block).getBrightness());
        //Casting has to be done for glass to render properly.
        baseModel.setColour(MLColors.VALID_COLORS[metadata].c.rgba()).computeNormals().render(new IconTransformation(block instanceof BlockLusGlass ? BlockLus.glassTexture : BlockLus.baseTexture));
        CCRenderState.draw();

        if (((BlockLus) block).hasColorMultiplier()) {
            MLColors.VALID_COLORS[metadata].c.glColour();
        }
        renderStandardInvBlock(renderer, block, metadata);
    }

    @Override
    public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer) {
        final Tessellator tess = Tessellator.instance;
        final int meta = world.getBlockMetadata(x, y, z);
        ((IBlockLus) block).setRGBColor(meta);
        tess.setBrightness(((IBlockLus) block).getBrightness(world, x, y, z));
        if (block instanceof BlockLusGlass) {
            renderer.setRenderBounds(0.00001, 0.00001, 0.00001, .99999, .99999, .99999);
        }
        //Casting has to be done for glass to render properly.
        renderAllSides(world, x, y, z, block, renderer, block instanceof BlockLusGlass ? BlockLus.glassTexture : BlockLus.baseTexture, false);
        renderer.setRenderBoundsFromBlock(block);
        renderer.renderStandardBlock(block, x, y, z);
        return true;
    }

    @Override
    public boolean shouldRender3DInInventory(int modelId) {
        return true;
    }
}
