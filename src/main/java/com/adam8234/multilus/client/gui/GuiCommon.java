package com.adam8234.multilus.client.gui;

import codechicken.lib.colour.ColourRGBA;
import codechicken.lib.render.CCRenderState;
import codechicken.lib.render.RenderUtils;
import codechicken.lib.vec.Vector3;
import com.adam8234.multilus.blocks.lus.BlockLus;
import com.adam8234.multilus.lib.cofh.gui.GuiBase;
import com.adam8234.multilus.lib.cofh.render.RenderHelper;
import com.adam8234.multilus.reference.Reference;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiCommon extends GuiBase {
    public static final ResourceLocation baseGui = new ResourceLocation(Reference.MOD_ID.toLowerCase(), "/textures/gui/guiBase.png");
    public GuiCommon(Container container) {
        super(container, baseGui);
        xSize = 176;
        ySize = 166;
    }

    public void drawProgressBar(int x, int y, double progress) {
        drawProgressBar(x, y, progress, new ColourRGBA(255, 255, 255, 255));
    }

    public void drawProgressBar(int x, int y, double progress, ColourRGBA colourRGBA) {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        bindTexture(baseGui);
        drawBar(x, y);
        double width = 25 * progress;
        RenderHelper.setBlockTextureSheet();
        colourRGBA.glColour();
        CCRenderState.startDrawing();
        RenderUtils.renderFluidQuad(new Vector3(x + 2, 2 + y, this.zLevel), new Vector3(x + 2, 8 + y, this.zLevel), new Vector3(x + 2 + width, 8 + y, this.zLevel), new Vector3(x + 2 + width, 2 + y, this.zLevel), BlockLus.baseTexture, 16.0D);
        CCRenderState.draw();
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
    }

    public void drawBar(int x, int y) {
        drawTexturedModalRect(x, y, 176, 0, 29, 10);
    }

}
