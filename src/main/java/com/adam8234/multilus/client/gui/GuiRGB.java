package com.adam8234.multilus.client.gui;

import codechicken.lib.colour.ColourRGBA;
import com.adam8234.multilus.inventory.ContainerRGB;
import com.adam8234.multilus.lib.cofh.gui.GuiColor;
import com.adam8234.multilus.lib.cofh.gui.element.listbox.SliderHorizontal;
import com.adam8234.multilus.lib.cofh.render.RenderHelper;
import com.adam8234.multilus.network.PacketHandler;
import com.adam8234.multilus.network.message.MessageTileCustomColorToServer;
import com.adam8234.multilus.reference.MLColors;
import com.adam8234.multilus.tile.TileEntityCustomColor;
import net.minecraft.entity.player.InventoryPlayer;


public class GuiRGB extends GuiCommon {
    private final TileEntityCustomColor tileEntityCustomColor;

    public GuiRGB(InventoryPlayer inventory, TileEntityCustomColor tileEntityCustomColor) {
        super(new ContainerRGB(inventory, tileEntityCustomColor));
        this.tileEntityCustomColor = tileEntityCustomColor;
        drawInventory = false;
    }

    @Override
    public void initGui() {
        super.initGui();
        SliderHorizontal sliderRed = new SliderHorizontal(this, 10, 30, xSize - 20, 10, 255) {
            @Override
            public void onValueChanged(int value) {
                tileEntityCustomColor.setR(value);
            }
        };
        SliderHorizontal sliderGreen = new SliderHorizontal(this, 10, 50, xSize - 20, 10, 255) {
            @Override
            public void onValueChanged(int value) {
                tileEntityCustomColor.setG(value);
            }
        };
        SliderHorizontal sliderBlue = new SliderHorizontal(this, 10, 70, xSize - 20, 10, 255) {
            @Override
            public void onValueChanged(int value) {
                tileEntityCustomColor.setB(value);
            }
        };
        sliderRed.setValue(tileEntityCustomColor.getR());
        sliderGreen.setValue(tileEntityCustomColor.getG());
        sliderBlue.setValue(tileEntityCustomColor.getB());
        sliderRed.setColor(new GuiColor(120, 120, 120, 255).getColor(), new GuiColor(60, 60, 60, 255).getColor());
        sliderGreen.setColor(new GuiColor(120, 120, 120, 255).getColor(), new GuiColor(60, 60, 60, 255).getColor());
        sliderBlue.setColor(new GuiColor(120, 120, 120, 255).getColor(), new GuiColor(60, 60, 60, 255).getColor());
        elements.add(sliderRed);
        elements.add(sliderGreen);
        elements.add(sliderBlue);

    }

    @Override
    protected void drawGuiContainerForegroundLayer(int x, int y) {
        fontRendererObj.drawStringWithShadow("Red:", 10, 21, MLColors.RED.rgb);
        fontRendererObj.drawStringWithShadow(String.valueOf(tileEntityCustomColor.getR()), 30, 21, MLColors.RED.rgb);
        fontRendererObj.drawStringWithShadow("Green:", 10, 41, MLColors.GREEN.rgb);
        fontRendererObj.drawStringWithShadow(String.valueOf(tileEntityCustomColor.getG()), 43, 41, MLColors.GREEN.rgb);
        fontRendererObj.drawStringWithShadow("Blue:", 10, 61, MLColors.BLUE.rgb);
        fontRendererObj.drawStringWithShadow(String.valueOf(tileEntityCustomColor.getB()), 34, 61, MLColors.BLUE.rgb);
        drawProgressBar(xSize / 2 - 14, 10, 1D, new ColourRGBA(tileEntityCustomColor.getR(), tileEntityCustomColor.getG(), tileEntityCustomColor.getB(), 255));
        super.drawGuiContainerForegroundLayer(x, y);
        RenderHelper.setBlockTextureSheet();
    }

    @Override
    public void onGuiClosed() {
        PacketHandler.instance.sendToServer(new MessageTileCustomColorToServer(tileEntityCustomColor));
        tileEntityCustomColor.updateBlock();
        super.onGuiClosed();
    }
}
