package com.adam8234.multilus.client.gui;

import com.adam8234.multilus.inventory.ContainerCrystalizer;
import com.adam8234.multilus.lib.cofh.gui.GuiBase;
import com.adam8234.multilus.reference.Reference;
import com.adam8234.multilus.tile.TileEntityCrystalizer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;

public class GuiCrystalizer extends GuiCommon{
    private final TileEntityCrystalizer tileEntityCrystalizer;

    public GuiCrystalizer(InventoryPlayer inventoryPlayer,TileEntityCrystalizer tileEntityCrystalizer) {
        super(new ContainerCrystalizer(inventoryPlayer, tileEntityCrystalizer));
        this.tileEntityCrystalizer = tileEntityCrystalizer;
        drawInventory = false;
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int x, int y) {
        drawProgressBar(xSize / 2 - 14, 40, 1D);
        super.drawGuiContainerForegroundLayer(x, y);
    }
}
