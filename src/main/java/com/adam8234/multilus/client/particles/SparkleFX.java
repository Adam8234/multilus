package com.adam8234.multilus.client.particles;

import com.adam8234.multilus.blocks.lus.BlockLus;
import com.adam8234.multilus.common.MLRepo;
import com.adam8234.multilus.reference.MLColors;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.world.World;

import java.awt.*;

public class SparkleFX extends EntityFX {
    public SparkleFX(World world, double x, double y, double z, double motionX, double motionY, double motionZ) {
        super(world, x, y, z, motionX, motionY, motionZ);
        setAlphaF(0.75F);
        setParticleIcon(BlockLus.particleIcon);
        particleMaxAge = 20;
    }

    public SparkleFX(World world, double x, double y, double z, double motionX, double motionY, double motionZ, int meta) {
        this(world, x, y, z, motionX, motionY, motionZ);
        final Color color = new Color(MLColors.VALID_COLORS[meta].rgb);
        setColors(color.getRed(), color.getGreen(), color.getBlue());
    }

    public SparkleFX(World world, double x, double y, double z, double motionX, double motionY, double motionZ, int r, int g, int b) {
        this(world, x, y, z, motionX, motionY, motionZ);
        setColors(r, g, b);
    }

    @Override
    public int getFXLayer() {
        return 1;
    }

    @Override
    public void onUpdate() {
        prevPosX = posX;
        prevPosY = posY;
        prevPosZ = posZ;

        if (particleAge++ >= particleMaxAge)
            setDead();

        float ageDifference = particleMaxAge - particleAge;
        if (ageDifference <= 10) {
            particleScale = ageDifference;
        } else {
            particleScale = particleAge;
        }
        particleScale = particleScale / (particleMaxAge / 2);

        if (onGround) {
            motionX *= 0.699999988079071D;
            motionZ *= 0.699999988079071D;
        }

    }

    private void setColors(int r, int g, int b) {
        particleRed = r / 256F;
        particleGreen = g / 256F;
        particleBlue = b / 256F;
    }

    @Override
    public void renderParticle(Tessellator par1Tessellator, float par2, float par3, float par4, float par5, float par6, float par7) {
        par1Tessellator.setBrightness(15 << 20 | 15 << 4);
        super.renderParticle(par1Tessellator, par2, par3, par4, par5, par6, par7);
    }
}
