package com.adam8234.multilus.client;

import com.adam8234.multilus.client.render.block.RenderCustomGlow;
import com.adam8234.multilus.client.render.block.RenderGlow;
import com.adam8234.multilus.client.render.block.RenderGlowMultiLayer;
import com.adam8234.multilus.client.render.item.ItemLusRenderer;
import com.adam8234.multilus.client.render.item.LusCrystalItemRenderer;
import com.adam8234.multilus.common.CommonProxy;
import com.adam8234.multilus.common.MLRepo;
import cpw.mods.fml.client.registry.RenderingRegistry;
import net.minecraftforge.client.MinecraftForgeClient;

public class ClientProxy extends CommonProxy {

    @Override
    public void preInit() {
    }

    @Override
    public void init() {
        RenderingRegistry.registerBlockHandler(new RenderGlow());
        RenderingRegistry.registerBlockHandler(new RenderCustomGlow());
        RenderingRegistry.registerBlockHandler(new RenderGlowMultiLayer());
        MinecraftForgeClient.registerItemRenderer(MLRepo.itemLusCrystal, new LusCrystalItemRenderer());
        MinecraftForgeClient.registerItemRenderer(MLRepo.itemLusInterface, new ItemLusRenderer());
    }

    @Override
    public void postInit() {

    }
}
