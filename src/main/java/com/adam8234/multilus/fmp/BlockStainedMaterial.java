package com.adam8234.multilus.fmp;

import codechicken.microblock.BlockMicroMaterial;
import com.adam8234.multilus.reference.MLColors;
import net.minecraft.block.Block;

public class BlockStainedMaterial extends BlockMicroMaterial {
    public BlockStainedMaterial(Block block, int meta) {
        super(block, meta);
    }

    @Override
    public int getColour(int pass) {
        return MLColors.VALID_COLORS[meta()].c.rgba();
    }
}