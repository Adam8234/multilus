package com.adam8234.multilus.recipe;

import com.adam8234.multilus.blocks.BlockSpecialStone.EnumSpecialStone;
import com.adam8234.multilus.common.MLBlocks;
import com.adam8234.multilus.common.MLRepo;
import com.adam8234.multilus.reference.EnumStained;
import com.adam8234.multilus.util.LogHelper;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

import static com.adam8234.multilus.blocks.BlockSpecialStone.EnumSpecialStone.*;
import static com.adam8234.multilus.common.MLBlocks.*;
import static com.adam8234.multilus.reference.EnumStained.*;
import static cpw.mods.fml.common.registry.GameRegistry.*;

public class RecipeHandler {

    public RecipeHandler() {
        initOreDict();
        registerBlockRecipes();
        registerItemRecipes();
    }

    private void registerItemRecipes() {
        addRecipe(new ItemStack(MLRepo.itemLusInterface), "bs ", "sgs", " sr", 'r', RED.getCrystal(), 'g', GREEN.getCrystal(), 'b', BLUE.getCrystal(), 's', DARK_STONE.getItemStack());

        for (EnumStained enumStained : EnumStained.VALID_TYPES) {
            addRecipe(new ItemStack(MLRepo.itemLusSword[enumStained.meta]), "c", "c", "s", 'c', enumStained.getCrystal(), 's', new ItemStack(Items.stick));
            addRecipe(new ItemStack(MLRepo.itemLusPickaxe[enumStained.meta]), "ccc", " s ", " s ", 'c', enumStained.getCrystal(), 's', new ItemStack(Items.stick));
            addRecipe(new ItemStack(MLRepo.itemLusShovel[enumStained.meta]), "c", "s", "s", 'c', enumStained.getCrystal(), 's', new ItemStack(Items.stick));
            addRecipe(new ItemStack(MLRepo.itemLusAxe[enumStained.meta]), "cc", "cs", " s", 'c', enumStained.getCrystal(), 's', new ItemStack(Items.stick));
        }
    }

    private void initOreDict() {
    }

    void registerBlockRecipes() {
        LogHelper.info("Registering Block Recipes");
        for (EnumStained enumStained : EnumStained.VALID_TYPES) {
            addRecipe(enumStained.getBlockItemStack(LUS_BRICK.block, 4), "x x", " c ", "x x", 'c', enumStained.getCrystal(), 'x', DARK_STONE_BRICK.getItemStack());
            addShapelessRecipe(enumStained.getCrystal(9), enumStained.getBlockItemStack(LUS_STORAGE.block));
            //addStairRecipe(new ItemStack(blockStainedStoneStairs[enumStained.meta], 4, 0), enumStained.getBlockItemStack(blockStainedBrick));
            addRecipe(enumStained.getBlockItemStack(LUS_FANCY_BRICK.block, 4), "xx", "xx", 'x', enumStained.getBlockItemStack(LUS_BRICK.block));
            addRecipe(enumStained.getBlockItemStack(LUS_HARD.block, 4), "xox", "oco", "xox", 'c', enumStained.getCrystal(), 'x', DARK_STONE.getItemStack(), 'o', new ItemStack(Blocks.obsidian, 1, 0));
            addRecipe(enumStained.getBlockItemStack(LUS_STORAGE.block, 4), "x x", " c ", "x x", 'c', enumStained.getCrystal(), 'x', DARK_STONE.getItemStack());
            addRecipe(enumStained.getBlockItemStack(CLEAR_GLASS.block, 4), "xcx", "c c", "xcx", 'c', new ItemStack(Blocks.glass, 1, 0), 'x', DARK_STONE.getItemStack());
            addRecipe(enumStained.getBlockItemStack(LUS_GLASS.block, 1), "c", "x", 'c', enumStained.getCrystal(), 'x', new ItemStack(CLEAR_GLASS.block, 1, 0));
            addRecipe(new ItemStack(LUS_CUSTOM.block, 4, 0), "sls", "rtb", "sgs", 'r', RED.getCrystal(), 'g', GREEN.getCrystal(), 'b', BLUE.getCrystal(), 'l', new ItemStack(Blocks.lever, 1, 0), 't', enumStained.getBlockItemStack(LUS_STORAGE.block), 's', DARK_STONE.getItemStack());
            addRecipe(new ItemStack(LUS_CUSTOM.block, 4, 1), "sls", "rtb", "sgs", 'r', RED.getCrystal(), 'g', GREEN.getCrystal(), 'b', BLUE.getCrystal(), 'l', new ItemStack(Blocks.lever, 1, 0), 't', enumStained.getBlockItemStack(LUS_BRICK.block), 's', DARK_STONE.getItemStack());
            addRecipe(new ItemStack(LUS_CUSTOM_GLASS.block, 4, 0), "sls", "rtb", "sgs", 'r', RED.getCrystal(), 'g', GREEN.getCrystal(), 'b', BLUE.getCrystal(), 'l', new ItemStack(Blocks.lever, 1, 0), 't', enumStained.getBlockItemStack(LUS_GLASS.block), 's', DARK_STONE.getItemStack());
            //addRecipe(new ShapedOreRecipe(enumStained.getBlockItemStack(blockStainedBrick), "xxx", "xdx", "xxx", 'x', new ItemStack(Blocks.stonebrick), 'd', enumStained.oreDic));
        }

        addRecipe(new ItemStack(BASIC_MACHINE.block, 1, 14), "srs", "mcm", "sms", 's', DARK_STONE.getItemStack(), 'c', ORANGE.getCrystal(), 'm', new ItemStack(Items.lava_bucket, 1, 0), 'r', new ItemStack(Items.redstone));
        addRecipe(new ItemStack(BASIC_MACHINE.block, 1, 11), "srs", "mcm", "sms", 's', DARK_STONE.getItemStack(), 'c', BLUE.getCrystal(), 'm', new ItemStack(Items.water_bucket, 1, 0), 'r', new ItemStack(Items.redstone));

        addSmelting(DARK_COBBLESTONE.getItemStack(), DARK_STONE.getItemStack(), 0.1F);
        addRecipe(DARK_STONE_BRICK.getItemStack(4), "xx", "xx", 'x', DARK_STONE.getItemStack());
        addRecipe(DARK_STONE_CARVED.getItemStack(4), "xx", "xx", 'x', DARK_STONE_BRICK.getItemStack());
        addShapelessRecipe(DARK_STONE_CARVED.getItemStack(), DARK_STONE_BRICK.getItemStack());
        addShapelessRecipe(DARK_STONE_BRICK.getItemStack(), DARK_STONE_CARVED.getItemStack());
        for (EnumSpecialStone enumSpecialStone : VALID_STONE) {
            addWallRecipe(new ItemStack(MLBlocks.SPECIAL_STONE_WALL.block, 6, enumSpecialStone.meta), enumSpecialStone.getItemStack());
        }
        /*
        for (EnumStairs enumStairs : EnumStairs.VALID_TYPES) {
            addStairRecipe(new ItemStack(mlStairs.get(enumStairs.ordinal()), 4, 0), new ItemStack(enumStairs.block, 0, enumStairs.meta));
        }
        */
    }

    void addWallRecipe(ItemStack o, ItemStack m) {
        addRecipe(o, "xxx", "xxx", 'x', m);
    }

    void addStairRecipe(ItemStack o, ItemStack m) {
        addRecipe(o, "x  ", "xx ", "xxx", 'x', m);
    }

    /*

            */

}