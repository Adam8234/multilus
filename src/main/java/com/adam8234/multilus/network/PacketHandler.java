package com.adam8234.multilus.network;

import com.adam8234.multilus.network.message.MessageTileCustomColorToClient;
import com.adam8234.multilus.network.message.MessageTileCustomColorToServer;
import com.adam8234.multilus.reference.Reference;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;

public class PacketHandler {
    public static final SimpleNetworkWrapper instance = NetworkRegistry.INSTANCE.newSimpleChannel(Reference.MOD_ID.toLowerCase());

    public static void init() {
        instance.registerMessage(MessageTileCustomColorToServer.class, MessageTileCustomColorToServer.class, 0, Side.SERVER);
        instance.registerMessage(MessageTileCustomColorToClient.class, MessageTileCustomColorToClient.class, 1, Side.CLIENT);
    }
}
