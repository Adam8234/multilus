package com.adam8234.multilus.network.message;

import com.adam8234.multilus.tile.TileEntityCustomColor;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.tileentity.TileEntity;

public class MessageTileCustomColorToServer implements IMessage, IMessageHandler<MessageTileCustomColorToServer, IMessage> {
    private int x;
    private int y;
    private int z;
    private int r;
    private int g;
    private int b;

    public MessageTileCustomColorToServer(){}

    public MessageTileCustomColorToServer(TileEntityCustomColor tileEntityCustomColor) {
        this.x = tileEntityCustomColor.xCoord;
        this.y = tileEntityCustomColor.yCoord;
        this.z = tileEntityCustomColor.zCoord;
        this.r = tileEntityCustomColor.getR();
        this.g = tileEntityCustomColor.getG();
        this.b = tileEntityCustomColor.getB();
    }

    /**
     * Convert from the supplied buffer into your specific message type
     *
     * @param buf
     */
    @Override
    public void fromBytes(ByteBuf buf) {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
        this.r = buf.readInt();
        this.g = buf.readInt();
        this.b = buf.readInt();
    }

    /**
     * Deconstruct your message into the supplied byte buffer
     *
     * @param buf
     */
    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(x);
        buf.writeInt(y);
        buf.writeInt(z);
        buf.writeInt(r);
        buf.writeInt(g);
        buf.writeInt(b);
    }

    /**
     * Called when a message is received of the appropriate type. You can optionally return a reply message, or null if no reply
     * is needed.
     *
     * @param message The message
     * @param ctx
     * @return an optional return message
     */
    @Override
    public IMessage onMessage(MessageTileCustomColorToServer message, MessageContext ctx) {
        TileEntity tileEntity = ctx.getServerHandler().playerEntity.worldObj.getTileEntity(message.x, message.y, message.z);
        if (tileEntity instanceof TileEntityCustomColor) {
            ((TileEntityCustomColor) tileEntity).setR(message.r);
            ((TileEntityCustomColor) tileEntity).setG(message.g);
            ((TileEntityCustomColor) tileEntity).setB(message.b);
            FMLClientHandler.instance().getClient().theWorld.func_147451_t(message.x, message.y, message.z);
            tileEntity.getWorldObj().markTileEntityChunkModified(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord, tileEntity);
        }
        return null;
    }

    @Override
    public String toString() {
        return String.format("MessageTileEntityCustomColor - x:%s, y:%s, z%s, r:%s, g:%s, b:%s", x, y, z, r, g, b);
    }


}
