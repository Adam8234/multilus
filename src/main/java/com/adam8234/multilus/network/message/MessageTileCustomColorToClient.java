package com.adam8234.multilus.network.message;

import com.adam8234.multilus.tile.TileEntityCustomColor;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.tileentity.TileEntity;

public class MessageTileCustomColorToClient implements IMessage, IMessageHandler<MessageTileCustomColorToClient, IMessage> {

    private int x, y, z;
    private int r, g, b;

    public MessageTileCustomColorToClient(){}

    public MessageTileCustomColorToClient(TileEntityCustomColor tileEntityCustomColor) {
        this.x = tileEntityCustomColor.xCoord;
        this.y = tileEntityCustomColor.yCoord;
        this.z = tileEntityCustomColor.zCoord;
        this.r = tileEntityCustomColor.getR();
        this.g = tileEntityCustomColor.getG();
        this.b = tileEntityCustomColor.getB();
    }

    @Override
    public IMessage onMessage(MessageTileCustomColorToClient message, MessageContext ctx) {
        TileEntity tileEntity = FMLClientHandler.instance().getClient().theWorld.getTileEntity(message.x, message.y, message.z);
        if (tileEntity instanceof TileEntityCustomColor) {
            ((TileEntityCustomColor) tileEntity).setR(message.r);
            ((TileEntityCustomColor) tileEntity).setG(message.g);
            ((TileEntityCustomColor) tileEntity).setB(message.b);
            FMLClientHandler.instance().getClient().theWorld.func_147451_t(message.x, message.y, message.z);
            ((TileEntityCustomColor) tileEntity).updateBlock();
        }
        return null;
    }

    /**
     * Convert from the supplied buffer into your specific message type
     *
     * @param buf
     */
    @Override
    public void fromBytes(ByteBuf buf) {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
        this.r = buf.readInt();
        this.g = buf.readInt();
        this.b = buf.readInt();
    }

    /**
     * Deconstruct your message into the supplied byte buffer
     *
     * @param buf
     */
    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(x);
        buf.writeInt(y);
        buf.writeInt(z);
        buf.writeInt(r);
        buf.writeInt(g);
        buf.writeInt(b);
    }
}

