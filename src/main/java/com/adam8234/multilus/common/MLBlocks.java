package com.adam8234.multilus.common;

import com.adam8234.multilus.blocks.BlockOre;
import com.adam8234.multilus.blocks.BlockSpecialStone;
import com.adam8234.multilus.blocks.BlockSpecialStoneWall;
import com.adam8234.multilus.blocks.item.ItemBlockColor;
import com.adam8234.multilus.blocks.item.ItemBlockMetaHandler;
import com.adam8234.multilus.blocks.item.ItemBlockSpecialStone;
import com.adam8234.multilus.blocks.item.ItemBlockSpecialStoneWalls;
import com.adam8234.multilus.blocks.item.lus.ItemBlockBasicMachine;
import com.adam8234.multilus.blocks.item.lus.ItemBlockLusCustom;
import com.adam8234.multilus.blocks.item.lus.ItemBlockLusHard;
import com.adam8234.multilus.blocks.lus.*;
import com.adam8234.multilus.blocks.lus.custom.BlockLusCustom;
import com.adam8234.multilus.blocks.lus.custom.BlockLusGlassCustom;
import com.adam8234.multilus.blocks.mech.BlockBasicMachine;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

import static com.adam8234.multilus.util.RegisterUtility.registerBlock;

public enum MLBlocks {
    SPECIAL_STONE(new BlockSpecialStone(), ItemBlockSpecialStone.class),
    SPECIAL_STONE_WALL(new BlockSpecialStoneWall().setHardness(1.5F), ItemBlockSpecialStoneWalls.class),
    //BLOCK_ORE(new BlockOre(), ItemBlockMetaHandler.class),
    LUS_ORE(new BlockLusOre(), ItemBlockColor.class),
    LUS_BRICK(new BlockLus(Material.rock, "lusBrick").setHasColorMultiplier(true), ItemBlockColor.class),
    LUS_HARD(new BlockLusConnectedMultiLayer("lusHard").setResistance(50), ItemBlockLusHard.class),
    LUS_STORAGE(new BlockLusConnected("lusStorage"), ItemBlockColor.class),
    //LUS_PLATFORM(new BlockLusConnectedMultiLayer("lusPlatform"), ItemBlockColor.class),
    //LUS_PLATFORM_HARD(new BlockLusConnectedMultiLayer("lusPlatformHard"), ItemBlockColor.class),
    LUS_FANCY_BRICK(new BlockLus(Material.rock, "lusFancyBrick"), ItemBlockColor.class),
    LUS_GLASS(new BlockLusGlass(), ItemBlockColor.class),
    CLEAR_GLASS(new BlockLusClearGlass(), ItemBlock.class),
    LUS_CUSTOM(new BlockLusCustom(), ItemBlockLusCustom.class),
    LUS_CUSTOM_GLASS(new BlockLusGlassCustom(), ItemBlockLusCustom.class),
    BASIC_MACHINE(new BlockBasicMachine(), ItemBlockBasicMachine.class),;
    //LUS_LAMP_ON(new BlockLusLamp(true), ItemBlockColor.class),
    //LUS_LAMP_OFF(new BlockLusLamp(false), ItemBlockColor.class);

    public static final MLBlocks[] VALID_TYPES = values();
    private final Class<? extends ItemBlock> itemBlock;
    public Block block;

    MLBlocks(Block block, Class<? extends ItemBlock> itemBlock) {
        this.block = block;
        this.itemBlock = itemBlock;
    }

    public void register() {
        registerBlock(block, itemBlock);
    }

    public ItemStack getItemStack(int i, int meta){
        return new ItemStack(block, i, meta);
    }

    public ItemStack getItemStack(int meta){
        return getItemStack(1, meta);
    }
}
