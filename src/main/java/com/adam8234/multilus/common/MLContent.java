package com.adam8234.multilus.common;

import com.adam8234.multilus.blocks.mech.BlockCrystalizer;
import com.adam8234.multilus.item.lus.ItemLusCrystal;
import com.adam8234.multilus.item.lus.ItemLusDust;
import com.adam8234.multilus.item.lus.tool.*;
import com.adam8234.multilus.reference.MLColors;
import com.adam8234.multilus.tile.TileEntityBasicMachine;
import com.adam8234.multilus.tile.TileEntityCrystalCluster;
import com.adam8234.multilus.tile.TileEntityCrystalizer;
import com.adam8234.multilus.tile.TileEntityCustomColor;
import com.adam8234.multilus.util.LogHelper;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;

import static com.adam8234.multilus.common.MLRepo.*;
import static com.adam8234.multilus.util.RegisterUtility.registerBlock;
import static cpw.mods.fml.common.registry.GameRegistry.registerTileEntity;

public class MLContent {
    public MLContent() {
        registerBlocks();
        registerItems();
    }

    void registerBlocks() {
        for(MLBlocks lusType: MLBlocks.VALID_TYPES){
            lusType.register();
        }
        //registerBlock(new BlockCrystalizer());
    }

    Item registerItem(Item item) {
        if ("item.null".equals(item.getUnlocalizedName()))
            throw new RuntimeException(String.format("Block is missing a proper name: %s", item.getClass().getName()));
        LogHelper.info("Registering Item: \"%s\" as \"%s\"", item.getClass().getSimpleName(), item.getUnlocalizedName().substring("item.".length()));
        GameRegistry.registerItem(item, item.getUnlocalizedName().substring("item.".length()));
        return item;
    }

    void registerItems() {
        itemLusCrystal = registerItem(new ItemLusCrystal());
        itemLusDust = registerItem(new ItemLusDust());
        itemLusInterface = registerItem(new ItemLusInterface());
        //itemLusWrench = registerItem(new ItemLusWrench());
        for (final MLColors e : MLColors.VALID_COLORS) {
            final Item itemSword = registerItem(new ItemLusSword(e));
            itemLusSword[e.meta] = itemSword;
        }

        for (final MLColors e : MLColors.VALID_COLORS) {
            final Item itemPickaxe = registerItem(new ItemLusPickaxe(e));
            itemLusPickaxe[e.meta] = itemPickaxe;
        }

        for (final MLColors e : MLColors.VALID_COLORS) {
            final Item itemAxe = registerItem(new ItemLusAxe(e));
            itemLusAxe[e.meta] = itemAxe;
        }

        for (final MLColors e : MLColors.VALID_COLORS) {
            final Item itemShovel = registerItem(new ItemLusShovel(e));
            itemLusShovel[e.meta] = itemShovel;
        }
    }

    public void registerMicroMaterials() {
        /*
        for (final BlockSpecialStone.EnumSpecialStone e : BlockSpecialStone.EnumSpecialStone.VALID_STONE) {
            registerMaterial(new BlockMicroMaterial(blockSpecialStone, e.meta), blockSpecialStone.getUnlocalizedName() + (e.meta > 0 ? "_" + e.meta : ""));
        }
        for (MLColors color : MLColors.VALID_COLORS) {
            registerMaterial(new BlockStainedMaterial(blockStainedBrick, color.meta), blockStainedBrick.getUnlocalizedName() + (color.meta > 0 ? "_" + color.meta : ""));
        }
        */
    }

    public void registerTileEntites() {
        registerTileEntity(TileEntityCustomColor.class, "tileColorCustomRGB");
        registerTileEntity(TileEntityBasicMachine.class, "tileWorld");
        //registerTileEntity(TileEntityCrystalizer.class, "tileCrystalizer");
    }
}
