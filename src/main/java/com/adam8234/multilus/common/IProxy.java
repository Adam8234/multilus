package com.adam8234.multilus.common;

public interface IProxy {
    public void preInit();

    public void init();

    public void postInit();
}
