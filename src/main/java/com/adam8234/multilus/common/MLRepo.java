package com.adam8234.multilus.common;

import com.adam8234.multilus.blocks.MLStair;
import net.minecraft.block.Block;
import net.minecraft.item.Item;

import java.util.ArrayList;

public class MLRepo {
    public static final Block[] blockStainedStoneStairs = new Block[16];

    public static Item itemLusInterface;
    public static Item itemLusCrystal;
    public static Item itemLusDust;
    public static final Item[] itemLusSword = new Item[16];
    public static final Item[] itemLusPickaxe = new Item[16];
    public static Item itemLusWrench;

    public static ArrayList<MLStair> mlStairs = new ArrayList<MLStair>();


    public static final Item[] itemLusAxe = new Item[16];
    public static final Item[] itemLusShovel = new Item[16];
}
