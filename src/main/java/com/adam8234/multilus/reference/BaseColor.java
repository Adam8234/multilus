package com.adam8234.multilus.reference;

public enum BaseColor {
    WHITE(MLColors.WHITE),
    GREEN(MLColors.GREEN),
    RED(MLColors.RED),
    BLUE(MLColors.BLUE),
    BLACK(MLColors.BLACK);

    public final MLColors color;
    public static final BaseColor[] VALID_TYPES = values();

    BaseColor(MLColors color){
        this.color = color;
    }

}
