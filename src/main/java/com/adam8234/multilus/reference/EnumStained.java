package com.adam8234.multilus.reference;

import com.adam8234.multilus.common.MLRepo;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;

public enum EnumStained {
    WHITE("White", null, MLColors.WHITE.getOreDict()),
    ORANGE("Orange", null, MLColors.ORANGE.getOreDict()),
    MAGENTA("Magenta", null, MLColors.MAGENTA.getOreDict()),
    LIGHT_BLUE("Light Blue", null, MLColors.LIGHT_BLUE.getOreDict()),
    YELLOW("Yellow", null, MLColors.YELLOW.getOreDict()),
    LIME("Lime", null, MLColors.LIME.getOreDict()),
    PINK("Pink", null, MLColors.PINK.getOreDict()),
    GREY("Grey", null, MLColors.GREY.getOreDict()),
    LIGHT_GREY("Light Grey", null, MLColors.LIGHT_GREY.getOreDict()),
    CYAN("Cyan", null, MLColors.CYAN.getOreDict()),
    PURPLE("Purple", null, MLColors.PURPLE.getOreDict()),
    BLUE("Blue", null, MLColors.BLUE.getOreDict()),
    BROWN("Brown", null, MLColors.BROWN.getOreDict()),
    GREEN("Green", null, MLColors.GREEN.getOreDict()),
    RED("Red", null, MLColors.RED.getOreDict()),
    BLACK("Black", null, MLColors.BLACK.getOreDict());
    public static final EnumStained[] VALID_TYPES = values();
    public final int meta = ordinal();
    public final String name;
    public final String oreDic;
    public final ItemStack specailDrop;

    private EnumStained(String name, ItemStack specailDrop, String oreDic) {
        this.name = name;
        this.specailDrop = specailDrop;
        this.oreDic = oreDic;
    }

    public ItemStack getBlockItemStack(Block block) {
        return getBlockItemStack(block, 1);
    }

    public ItemStack getBlockItemStack(Block block, int i) {
        return new ItemStack(block, i, meta);
    }

    public ItemStack getCrystal() {
        return getCrystal(1);
    }

    public ItemStack getCrystal(int i) {
        return new ItemStack(MLRepo.itemLusCrystal, i, meta);
    }

    public ItemStack getDust() {
        return getDust(1);
    }

    public ItemStack getDust(int i) {
        return new ItemStack(MLRepo.itemLusDust, i, meta);
    }

}
