package com.adam8234.multilus.reference;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;

public enum EnumStairs {
    EMERALD(Blocks.emerald_block, 0),
    DIAMOND(Blocks.diamond_block, 0),
    LAPIS(Blocks.lapis_block, 0),
    IRON_BLOCK(Blocks.iron_block, 0),
    REDSTONE_BLOCK(Blocks.redstone_block, 0),
    COAL_BLOCK(Blocks.coal_block, 0),
    PACKED_ICE(Blocks.packed_ice, 0),
    GLOW_STONE(Blocks.glowstone, 0),
    SNOW(Blocks.snow, 0),
    MOSSY_STONE(Blocks.mossy_cobblestone, 0),
    STONE(Blocks.stone, 0),
    END_STONE(Blocks.end_stone, 0),
    HAY_BALE(Blocks.hay_block, 0),
    HARDENED_CLAY(Blocks.hardened_clay, 0),

    WHITE_STAINED_CLAY(Blocks.stained_hardened_clay, 0),
    ORANGE_STAINED_CLAY(Blocks.stained_hardened_clay, 1),
    MAGENTA_STAINED_CLAY(Blocks.stained_hardened_clay, 2),
    LIGHT_BLUE_STAINED_CLAY(Blocks.stained_hardened_clay, 3),
    YELLOW_STAINED_CLAY(Blocks.stained_hardened_clay, 4),
    LIME_STAINED_CLAY(Blocks.stained_hardened_clay, 5),
    PINK_STAINED_CLAY(Blocks.stained_hardened_clay, 6),
    GREY_STAINED_CLAY(Blocks.stained_hardened_clay, 7),
    LIGHT_GREY_STAINED_CLAY(Blocks.stained_hardened_clay, 8),
    CYAN_STAINED_CLAY(Blocks.stained_hardened_clay, 9),
    PURPLE_STAINED_CLAY(Blocks.stained_hardened_clay, 10),
    BLUE_STAINED_CLAY(Blocks.stained_hardened_clay, 11),
    BROWN_STAINED_CLAY(Blocks.stained_hardened_clay, 12),
    GREEN_STAINED_CLAY(Blocks.stained_hardened_clay, 13),
    RED_STAINED_CLAY(Blocks.stained_hardened_clay, 14),
    BLACK_STAINED_CLAY(Blocks.stained_hardened_clay, 15),

    WHITE_WOOL(Blocks.wool, 0),
    ORANGE_WOOL(Blocks.wool, 1),
    MAGENTA_WOOL(Blocks.wool, 2),
    LIGHT_BLUE_WOOL(Blocks.wool, 3),
    YELLOW_WOOL(Blocks.wool, 4),
    LIME_WOOL(Blocks.wool, 5),
    PINK_WOOL(Blocks.wool, 6),
    GREY_WOOL(Blocks.wool, 7),
    LIGHT_GREY_WOOL(Blocks.wool, 8),
    CYAN_WOOL(Blocks.wool, 9),
    PURPLE_WOOL(Blocks.wool, 10),
    BLUE_WOOL(Blocks.wool, 11),
    BROWN_WOOL(Blocks.wool, 12),
    GREEN_WOOL(Blocks.wool, 13),
    RED_WOOL(Blocks.wool, 14),
    BLACK_WOOL(Blocks.wool, 15),;
    public static final EnumStairs[] VALID_TYPES = values();
    public final Block block;
    public final int meta;

    private EnumStairs(Block block, int meta) {
        this.block = block;
        this.meta = meta;
    }
}
