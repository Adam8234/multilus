package com.adam8234.multilus.generator;

import com.adam8234.multilus.blocks.BlockSpecialStone;
import com.adam8234.multilus.lib.cofh.util.WeightedRandomBlock;
import com.adam8234.multilus.lib.cofh.world.feature.WorldGenMinableCluster;
import com.adam8234.multilus.reference.EnumStained;
import com.adam8234.multilus.common.MLBlocks;
import cpw.mods.fml.common.IWorldGenerator;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.ChunkProviderEnd;
import net.minecraft.world.gen.ChunkProviderFlat;
import net.minecraft.world.gen.ChunkProviderHell;

import java.util.ArrayList;
import java.util.Random;

public class WorldGeneratorManager implements IWorldGenerator{
    public static final WorldGeneratorManager instance = new WorldGeneratorManager();
    public static WeightedRandomBlock DARK_STONE = new WeightedRandomBlock(BlockSpecialStone.EnumSpecialStone.DARK_STONE.getItemStack(), 75);
    @Override
    public void generate(Random r, int chunkX, int chunkZ, World w, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
        if (chunkGenerator instanceof ChunkProviderHell || chunkGenerator instanceof ChunkProviderEnd || chunkGenerator instanceof ChunkProviderFlat)
            return;

        if (w.provider.dimensionId == -1 || w.provider.dimensionId == 1)
            return;

        if (true) {
            ArrayList<WeightedRandomBlock> randomBlock = new ArrayList<WeightedRandomBlock>();
            randomBlock.add(new WeightedRandomBlock(EnumStained.VALID_TYPES[r.nextInt(15)].getBlockItemStack(MLBlocks.LUS_ORE.block), 25));
            randomBlock.add(DARK_STONE);
            final int x = chunkX * 16 + r.nextInt(16);
            final int y = 20 + r.nextInt(40);
            final int z = chunkZ * 16 + r.nextInt(16);
            new WorldGenMinableCluster(randomBlock, 20).generate(w, r, x, y, z);
        }

    }
}