package com.adam8234.multilus.tile;

import codechicken.lib.colour.ColourRGBA;
import com.adam8234.multilus.network.PacketHandler;
import com.adam8234.multilus.network.message.MessageTileCustomColorToClient;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.Packet;
import net.minecraft.tileentity.TileEntity;

public class TileEntityCustomColor extends TileEntity implements IInventory {
    private final int increment = 5;
    //Default Color when you place it;
    private int r = 255;
    private int g = 255;
    private int b = 255;

    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }

    public int getG() {
        return g;
    }

    public void setG(int g) {
        this.g = g;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getColor() {
        return new ColourRGBA(r, g, b, 255).rgb();
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        r = nbt.getInteger("r");
        g = nbt.getInteger("g");
        b = nbt.getInteger("b");
        PacketHandler.instance.sendToAll(new MessageTileCustomColorToClient(this));
    }

    @Override
    public void writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        nbt.setInteger("r", r);
        nbt.setInteger("g", g);
        nbt.setInteger("b", b);
    }

    @Override
    public Packet getDescriptionPacket() {
        return PacketHandler.instance.getPacketFrom(new MessageTileCustomColorToClient(this));
    }

    @Override
    public int getSizeInventory() {
        return 0;
    }

    @Override
    public ItemStack getStackInSlot(int i) {
        return null;
    }

    @Override
    public ItemStack decrStackSize(int i, int i2) {
        return null;
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int i) {
        return null;
    }

    @Override
    public void setInventorySlotContents(int i, ItemStack itemStack) {

    }

    @Override
    public String getInventoryName() {
        return null;
    }

    @Override
    public boolean hasCustomInventoryName() {
        return false;
    }

    @Override
    public int getInventoryStackLimit() {
        return 0;
    }

    @Override
    public boolean isUseableByPlayer(EntityPlayer entityPlayer) {
        return false;
    }

    @Override
    public void openInventory() {

    }

    public void updateBlock() {
        if (worldObj.isRemote) {
            worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
        }
    }

    @Override
    public void closeInventory() {
    }

    @Override
    public boolean isItemValidForSlot(int i, ItemStack itemStack) {
        return false;
    }
}
