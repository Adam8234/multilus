package com.adam8234.multilus.tile;

import com.adam8234.multilus.blocks.mech.BlockBasicMachine;
import com.adam8234.multilus.lib.cofh.util.ServerHelper;
import com.adam8234.multilus.util.LogHelper;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

public class TileEntityBasicMachine extends TileEntity implements ISidedInventory {
    public boolean hasCobbleGen;
    private int meta;
    public TileEntityBasicMachine(){
        this.hasCobbleGen = updateMultiBlock();
    }
    public TileEntityBasicMachine(int meta) {
        this.meta = meta;
    }

    @Override
    public void updateEntity() {
        if(ServerHelper.isServerWorld(worldObj)) {
            this.hasCobbleGen = updateMultiBlock();
        }
        super.updateEntity();
    }

    @Override
    public int[] getAccessibleSlotsFromSide(int p_94128_1_) {
        return new int[6];
    }

    @Override
    public boolean canInsertItem(int p_102007_1_, ItemStack p_102007_2_, int p_102007_3_) {
        return false;
    }

    @Override
    public boolean canExtractItem(int p_102008_1_, ItemStack p_102008_2_, int p_102008_3_) {
        return hasCobbleGen;
    }

    @Override
    public int getSizeInventory() {
        return 1;
    }

    @Override
    public ItemStack getStackInSlot(int p_70301_1_) {
        return new ItemStack(Blocks.cobblestone);
    }

    @Override
    public ItemStack decrStackSize(int p_70298_1_, int p_70298_2_) {
        return new ItemStack(Blocks.cobblestone);
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int p_70304_1_) {
        return null;
    }

    @Override
    public void setInventorySlotContents(int p_70299_1_, ItemStack p_70299_2_) {
    }

    @Override
    public String getInventoryName() {
        return null;
    }

    @Override
    public boolean hasCustomInventoryName() {
        return false;
    }

    @Override
    public int getInventoryStackLimit() {
        return 0;
    }

    @Override
    public boolean isUseableByPlayer(EntityPlayer p_70300_1_) {
        return false;
    }

    @Override
    public void openInventory() {

    }

    @Override
    public void closeInventory() {

    }

    @Override
    public boolean isItemValidForSlot(int p_94041_1_, ItemStack p_94041_2_) {
        return false;
    }

    public boolean updateMultiBlock() {
        for(ForgeDirection side: ForgeDirection.VALID_DIRECTIONS){
            if(worldObj.getBlock(xCoord + side.offsetX, yCoord + side.offsetY, zCoord + side.offsetZ) instanceof BlockBasicMachine){
                 if(isOppisiteBlock(side)){
                     return true;
                 }
            }
        }
        return false;
    }

    public boolean isOppisiteBlock(ForgeDirection side){
        return worldObj.getBlockMetadata(xCoord, yCoord, zCoord) != worldObj.getBlockMetadata(xCoord + side.offsetX, yCoord + side.offsetY, zCoord + side.offsetZ) ? true : false;
    }

}
