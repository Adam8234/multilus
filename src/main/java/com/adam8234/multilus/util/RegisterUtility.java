package com.adam8234.multilus.util;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import static cpw.mods.fml.common.registry.GameRegistry.registerTileEntity;

public class RegisterUtility {
    public static Block registerBlock(Block block, Class<? extends ItemBlock> itemBlock) {
        if ("tile.null".equals(block.getUnlocalizedName()))
            throw new RuntimeException(String.format("Block is miising a proper name: %s", block.getClass().getName()));
        LogHelper.info("Registering Block: \"%s\" as \"%s\" with item \"%s\"", block.getClass().getSimpleName(), block.getUnlocalizedName().substring("tile.".length()), itemBlock.getSimpleName());
        return GameRegistry.registerBlock(block, itemBlock, block.getUnlocalizedName().substring("tile.".length()));
    }

    public static Block registerBlock(Block block) {
        return registerBlock(block, ItemBlock.class);
    }

    public static Item registerItem(Item item) {
        if ("item.null".equals(item.getUnlocalizedName()))
            throw new RuntimeException(String.format("Block is missing a proper name: %s", item.getClass().getName()));
        LogHelper.info("Registering Item: \"%s\" as \"%s\"", item.getClass().getSimpleName(), item.getUnlocalizedName().substring("item.".length()));
        GameRegistry.registerItem(item, item.getUnlocalizedName().substring("item.".length()));
        return item;
    }
}
