package com.adam8234.multilus.blocks;

import com.adam8234.multilus.MultiLus;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockStairs;
import net.minecraft.world.IBlockAccess;

public class MLStair extends BlockStairs {


    private final Block block;

    public MLStair(Block block, int meta) {
        super(block, meta);
        setCreativeTab(MultiLus.tabBlock);
        this.block = block;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public int getLightValue() {
        return block.getLightValue();
    }

    @Override
    public boolean canConnectRedstone(IBlockAccess world, int x, int y, int z, int side) {
        return block.canConnectRedstone(world, x, y, z, side);
    }

    @Override
    public boolean canProvidePower() {
        return block.canProvidePower();
    }

    @Override
    public int isProvidingStrongPower(IBlockAccess world, int x, int y, int z, int side) {
        return canProvidePower() && canConnectRedstone(world, x, y, z, side) ? 15 : 0;
    }
}
