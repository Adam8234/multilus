package com.adam8234.multilus.blocks;

import com.adam8234.multilus.MultiLus;
import com.adam8234.multilus.common.MLBlocks;
import com.adam8234.multilus.reference.Reference;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;

public class BlockSpecialStone extends Block {
    public BlockSpecialStone() {
        super(Material.rock);
        setBlockName("obfuscousStone");
        setHardness(3.0F);
        setResistance(10.0F);
        setCreativeTab(MultiLus.tabBlock);
    }

    @Override
    public int damageDropped(int meta) {
        return meta;
    }

    @Override
    public float getBlockHardness(World world, int x, int y, int z) {
        final int meta = world.getBlockMetadata(x, y, z);
        return EnumSpecialStone.VALID_STONE[meta].hardness;
    }

    @Override
    public ArrayList<ItemStack> getDrops(World world, int x, int y, int z, int meta, int fortune) {
        ArrayList<ItemStack> drops = new ArrayList<ItemStack>();
        EnumSpecialStone type = EnumSpecialStone.VALID_STONE[meta];
        drops.add(type.drop == null ? type.getItemStack() : type.drop.copy());
        return drops;
    }

    @Override
    public float getExplosionResistance(Entity exploder, World world, int x, int y, int z, double srcX, double srcY, double srcZ) {
        final int meta = world.getBlockMetadata(x, y, z);
        return EnumSpecialStone.VALID_STONE[meta].explosionRes;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int meta) {
        return EnumSpecialStone.VALID_STONE[meta].texture;
    }

    @Override
    public void getSubBlocks(Item item, CreativeTabs tab, List list) {
        for (final EnumSpecialStone s : EnumSpecialStone.VALID_STONE)
            list.add(s.getItemStack());
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister reg) {
        for (final EnumSpecialStone s : EnumSpecialStone.VALID_STONE)
            s.loadTextures(reg);
    }

    public enum EnumSpecialStone {
        DARK_COBBLESTONE("blockObfuscousCobblestone", 3, 10, null),
        DARK_STONE_BRICK("blockObfuscousStoneBrick", 3, 10, null),
        DARK_STONE_CARVED("blockObfuscousCarvedBrick", 3, 10, null),
        DARK_STONE("blockObfuscousStone", 3, 10, DARK_COBBLESTONE.getItemStack()),;

        public static final EnumSpecialStone[] VALID_STONE = values();
        public final float explosionRes;
        public final float hardness;
        public final int meta = ordinal();
        public final ItemStack drop;
        public final String unlocal;
        public IIcon texture;

        private EnumSpecialStone(String unlocal, float hardness, float explosionRes, ItemStack drop) {
            this.unlocal = unlocal;
            this.hardness = hardness;
            this.explosionRes = explosionRes;
            this.drop = drop;
        }

        public ItemStack getItemStack() {
            return getItemStack(1);
        }

        public ItemStack getItemStack(int i) {
            return new ItemStack(MLBlocks.SPECIAL_STONE.block, i, meta);
        }

        public void loadTextures(IIconRegister reg) {
            texture = reg.registerIcon(Reference.MOD_ID.toLowerCase() + ":stone/" + unlocal);
        }
    }
}
