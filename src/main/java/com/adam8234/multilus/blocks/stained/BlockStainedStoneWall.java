package com.adam8234.multilus.blocks.stained;

import com.adam8234.multilus.MultiLus;
import com.adam8234.multilus.reference.EnumStained;
import com.adam8234.multilus.reference.MLColors;
import com.adam8234.multilus.reference.Reference;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockWall;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;

import java.util.List;

public class BlockStainedStoneWall extends BlockWall {

    public BlockStainedStoneWall() {
        super(Block.getBlockFromName("stone"));
        setBlockName("stainedStoneWalls");
        setCreativeTab(MultiLus.tabBlock);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public int colorMultiplier(IBlockAccess w, int x, int y, int z) {
        return MLColors.get(w.getBlockMetadata(x, y, z)).rgb;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int par1, int par2) {
        return blockIcon;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public int getRenderColor(int meta) {
        return MLColors.get(meta).rgb;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void getSubBlocks(Item item, CreativeTabs tab, List list) {
        for (final EnumStained t : EnumStained.VALID_TYPES)
            list.add(t.getBlockItemStack(this));
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister reg) {
        blockIcon = reg.registerIcon(Reference.MOD_ID.toLowerCase() + ":dyeStone");
    }
}
