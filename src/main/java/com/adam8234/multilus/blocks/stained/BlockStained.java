package com.adam8234.multilus.blocks.stained;

import com.adam8234.multilus.MultiLus;
import com.adam8234.multilus.reference.EnumStained;
import com.adam8234.multilus.reference.MLColors;
import com.adam8234.multilus.reference.Reference;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import java.util.List;

public class BlockStained extends Block {

    private final String texture;

    public BlockStained(Material mat, String texture) {
        super(mat);
        setBlockName(texture);
        this.texture = texture;
        setHardness(1.5F);
        setResistance(30);
        setCreativeTab(MultiLus.tabBlock);
    }

    @Override
    public int damageDropped(int meta) {
        return meta;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public int colorMultiplier(IBlockAccess w, int x, int y, int z) {
        return MLColors.get(w.getBlockMetadata(x, y, z)).rgb;
    }

    @Override
    public ItemStack getPickBlock(MovingObjectPosition target, World world, int x, int y, int z) {
        return EnumStained.VALID_TYPES[world.getBlockMetadata(x, y, z)].getBlockItemStack(this);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public int getRenderColor(int meta) {
        return MLColors.get(meta).rgb;
    }

    @Override
    public void getSubBlocks(Item item, CreativeTabs tab, List list) {
        for (final EnumStained t : EnumStained.VALID_TYPES)
            list.add(t.getBlockItemStack(this));
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister reg) {
        blockIcon = reg.registerIcon(Reference.MOD_ID.toLowerCase() + ":" + texture);
    }
}
