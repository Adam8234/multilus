package com.adam8234.multilus.blocks.stained;

import com.adam8234.multilus.MultiLus;
import com.adam8234.multilus.reference.MLColors;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockStairs;
import net.minecraft.world.IBlockAccess;

public class BlockStainedStairs extends BlockStairs {

    private final MLColors e;

    public BlockStainedStairs(Block block, MLColors e) {
        super(block, 1);
        setCreativeTab(MultiLus.tabBlock);
        this.e = e;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public int colorMultiplier(IBlockAccess w, int x, int y, int z) {
        return e.rgb;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public int getRenderColor(int meta) {
        return e.rgb;
    }

}
