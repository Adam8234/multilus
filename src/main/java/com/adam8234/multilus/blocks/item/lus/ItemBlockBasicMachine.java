package com.adam8234.multilus.blocks.item.lus;

import com.adam8234.multilus.blocks.item.ItemBlockMetaHandler;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;

import java.util.List;

public class ItemBlockBasicMachine extends ItemBlockMetaHandler {

    public ItemBlockBasicMachine(Block block) {
        super(block);
    }

    @Override
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
        switch (par1ItemStack.getItemDamage()) {
            case 0:
                par3List.add(EnumChatFormatting.WHITE + "Replaces Water With Ice");
                break;
            case 1:
                par3List.add(EnumChatFormatting.BLUE + "Replaces Lava With With Obsidian or Cobblestone");
                break;
            case 2:
                par3List.add(EnumChatFormatting.RED + "Replaces Water With Stone");
                break;
        }
        super.addInformation(par1ItemStack, par2EntityPlayer, par3List, par4);
    }
}
