package com.adam8234.multilus.blocks.item.lus;

import com.adam8234.multilus.blocks.item.ItemBlockColor;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;

import java.util.List;

public class ItemBlockLusHard extends ItemBlockColor {

    public ItemBlockLusHard(Block block) {
        super(block);
    }

    @Override
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
        super.addInformation(par1ItemStack, par2EntityPlayer, par3List, par4);
        par3List.add(EnumChatFormatting.RED + "Resistant To Explosions");
    }

}
