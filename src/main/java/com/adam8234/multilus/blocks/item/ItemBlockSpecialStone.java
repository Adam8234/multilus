package com.adam8234.multilus.blocks.item;

import com.adam8234.multilus.blocks.BlockSpecialStone.EnumSpecialStone;
import com.adam8234.multilus.common.MLBlocks;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;

public class ItemBlockSpecialStone extends ItemBlockMetaHandler {
    public ItemBlockSpecialStone(Block block) {
        super(block);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public String getUnlocalizedName(ItemStack itemstack) {
        return MLBlocks.SPECIAL_STONE.block.getUnlocalizedName() + "." + EnumSpecialStone.VALID_STONE[itemstack.getItemDamage()].unlocal;
    }

}
