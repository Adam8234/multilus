package com.adam8234.multilus.blocks.item.lus;

import com.adam8234.multilus.blocks.item.ItemBlockMetaHandler;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;

import java.util.List;

public class ItemBlockLusCustom extends ItemBlockMetaHandler {
    public ItemBlockLusCustom(Block block) {
        super(block);
    }

    @Override
    public void addInformation(ItemStack itemStack, EntityPlayer player, List list, boolean par4) {
        list.add(EnumChatFormatting.GREEN + "Shift right click with empty hand to edit color");
        super.addInformation(itemStack, player, list, par4);
    }
}
