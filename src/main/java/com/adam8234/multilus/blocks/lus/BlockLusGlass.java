package com.adam8234.multilus.blocks.lus;

import com.adam8234.multilus.common.MLBlocks;
import com.adam8234.multilus.reference.Reference;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;

public class BlockLusGlass extends BlockLus {

    public BlockLusGlass() {
        super(Material.glass);
        setHardness(0.3F);
        setResistance(1.5F);
        setBlockName("blockLusGlass");
    }

    @Override
    public IIcon getIcon(IBlockAccess world, int x, int y, int z, int side) {
        return BlockLusConnected.getConnectedBlockTexture(world, x, y, z, side, this);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int par1, int par2) {
        return MLBlocks.LUS_STORAGE.block.getIcon(par1, par2);
    }

    @Override
    public int getRenderBlockPass() {
        return 1;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public void registerBlockIcons(IIconRegister reg) {
        baseTexture = reg.registerIcon(Reference.MOD_ID.toLowerCase() + ":overlay/dyeGlassBase");
    }

    @Override
    @SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockAccess world, int x, int y, int z, int side) {
        final Block block = world.getBlock(x, y, z);
        return block != this && super.shouldSideBeRendered(world, x, y, z, side);
    }

}
