package com.adam8234.multilus.blocks.lus.custom;

import com.adam8234.multilus.MultiLus;
import com.adam8234.multilus.blocks.lus.BlockLus;
import com.adam8234.multilus.blocks.lus.BlockLusConnected;
import com.adam8234.multilus.client.render.block.RenderCustomGlow;
import com.adam8234.multilus.common.MLBlocks;
import com.adam8234.multilus.handler.GuiHandler;
import com.adam8234.multilus.item.lus.tool.ItemLusInterface;
import com.adam8234.multilus.reference.Reference;
import com.adam8234.multilus.tile.TileEntityCustomColor;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import java.util.List;

public class BlockLusCustom extends BlockLus implements IBlockLusCustom, ITileEntityProvider {
    private final IIcon[] icons = new IIcon[4];

    public BlockLusCustom() {
        this(Material.rock);
        setBlockName("lusCustom");
    }

    public BlockLusCustom(Material material) {
        super(material);
    }

    @Override
    public TileEntity createNewTileEntity(World world, int meta) {
        return new TileEntityCustomColor();
    }

    @Override
    public IIcon getIcon(IBlockAccess world, int x, int y, int z, int side) {
        switch (world.getBlockMetadata(x, y, z)) {
            case 0:
                return BlockLusConnected.getConnectedBlockTexture(world, x, y, z, side, this);
            default:
                return icons[world.getBlockMetadata(x, y, z)];
        }

    }


    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9) {
        if (player.getHeldItem() != null) {
            return player.getHeldItem().getItem() instanceof ItemLusInterface;
        }
        if (player.isSneaking()) {
            if (!world.isRemote && world.getTileEntity(x, y, z) instanceof TileEntityCustomColor) {
                player.openGui(MultiLus.instance, GuiHandler.GuiIds.RGB.id, world, x, y, z);
            }
            return true;
        }
        return true;
    }

    @Override
    public IIcon getIcon(int side, int meta) {
        return icons[meta];
    }

    @Override
    public int getRenderType() {
        return RenderCustomGlow.modelId;
    }

    @Override
    public void getRGBColor(IBlockAccess world, int x, int y, int z, Tessellator tess) {
        final TileEntityCustomColor tile = (TileEntityCustomColor) world.getTileEntity(x, y, z);
        final int r = tile.getR();
        final int g = tile.getG();
        final int b = tile.getB();
        tess.setColorRGBA(r, g, b, 255);
    }

    @Override
    public void getSubBlocks(Item item, CreativeTabs tab, List list) {
        for (int i = 0; i < 2; i++)
            list.add(new ItemStack(this, 1, i));
    }

    @Override
    public void registerBlockIcons(IIconRegister reg) {
        icons[0] = reg.registerIcon(Reference.MOD_ID.toLowerCase() + ":overlay/lusStorage/dyeGlow");
        icons[1] = reg.registerIcon(Reference.MOD_ID.toLowerCase() + ":overlay/lusBrick");
        super.registerBlockIcons(reg);
    }

    @Override
    public int colorMultiplier(IBlockAccess world, int x, int y, int z) {
        return world.getBlockMetadata(x, y, z) == 1 ? ((TileEntityCustomColor) world.getTileEntity(x, y, z)).getColor() : super.colorMultiplier(world, x, y, z);
    }
}
