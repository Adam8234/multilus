package com.adam8234.multilus.blocks.lus;

import com.adam8234.multilus.reference.BaseColor;
import com.adam8234.multilus.reference.EnumStained;
import com.adam8234.multilus.reference.MLColors;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import scala.xml.dtd.impl.Base;

import java.util.ArrayList;
import java.util.List;

public class BlockLusOre extends BlockLus {
    private final int min = 4;
    private final int max = 12;

    public BlockLusOre() {
        super(Material.rock, "lusOre");
        setHardness(2.0F);
        setResistance(10.0F);
        setHarvestLevel("pickaxe", 2);
        setBlockName("dyeOre");
        setHardness(3F);
    }

    @Override
    public ArrayList<ItemStack> getDrops(World world, int x, int y, int z, int metadata, int fortune) {
        ArrayList<ItemStack> ret = new ArrayList<ItemStack>();
        final int count = min + world.rand.nextInt(max - min);
        dropXpOnBlockBreak(world, x, y, z, MathHelper.getRandomIntegerInRange(world.rand, 2, 5));
        ret.add(EnumStained.VALID_TYPES[metadata].getCrystal(count));
        return ret;
    }
}
