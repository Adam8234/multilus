package com.adam8234.multilus.blocks.lus;

import com.adam8234.multilus.client.render.block.RenderGlowMultiLayer;
import com.adam8234.multilus.reference.Reference;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;

public class BlockLusConnectedMultiLayer extends BlockLusConnected{
    private final String texture;
    private IIcon layer;

    public BlockLusConnectedMultiLayer(String texture) {
        super("lusStorage");
        setBlockName(texture);
        this.texture = texture;
    }

    @Override
    public int getRenderType() {
        return RenderGlowMultiLayer.modelId;
    }

    @Override
    public void registerBlockIcons(IIconRegister reg) {
        layer = reg.registerIcon(Reference.MOD_ID.toLowerCase() + ":overlay/" + texture);
        super.registerBlockIcons(reg);
    }

    public IIcon getLayer() {
        return layer;
    }

}
