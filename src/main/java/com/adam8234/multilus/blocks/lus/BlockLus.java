package com.adam8234.multilus.blocks.lus;

import com.adam8234.multilus.MultiLus;
import com.adam8234.multilus.blocks.BlockML;
import com.adam8234.multilus.blocks.lus.custom.IBlockLusCustom;
import com.adam8234.multilus.client.particles.SparkleFX;
import com.adam8234.multilus.client.render.block.RenderGlow;
import com.adam8234.multilus.common.MLRepo;
import com.adam8234.multilus.reference.EnumStained;
import com.adam8234.multilus.reference.MLColors;
import com.adam8234.multilus.reference.Reference;
import com.adam8234.multilus.tile.TileEntityCustomColor;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.EffectRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

import java.util.List;
import java.util.Random;

public class BlockLus extends BlockML implements IBlockLus {

    public static IIcon particleIcon;
    public static IIcon baseTexture = null;
    private boolean hasColorMultiplier;
    public static IIcon glassTexture;

    public BlockLus(Material material) {
        super(material);
        setCreativeTab(MultiLus.tabBlock);
        setHardness(1F);
        setResistance(15F);
    }

    public BlockLus(Material material, String name) {
        this(material);
        this.setBlockTextureName(name);
        this.setBlockName(name);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public boolean addDestroyEffects(World world, int x, int y, int z, int meta, EffectRenderer effectRenderer) {
        TileEntityCustomColor tile;
        if (world.getBlock(x, y, z) instanceof IBlockLusCustom)
            tile = (TileEntityCustomColor) world.getTileEntity(x, y, z);
        else
            tile = null;
        for (int i = 0; i < 30; i++) {
            final float particleX = x + world.rand.nextFloat();
            final float particleY = y + world.rand.nextFloat();
            final float particleZ = z + world.rand.nextFloat();
            if (tile != null)
                Minecraft.getMinecraft().effectRenderer.addEffect(new SparkleFX(world, particleX, particleY, particleZ, 0, 0, 0, tile.getR(), tile.getG(), tile.getB()));
            else
                Minecraft.getMinecraft().effectRenderer.addEffect(new SparkleFX(world, particleX, particleY, particleZ, 0, 0, 0, world.getBlockMetadata(x, y, z)));
        }
        return false;
    }

    @SideOnly(Side.CLIENT)
    public void renderParticlesAllSides(World world, ForgeDirection side, Random rand, int x, int y, int z) {
        TileEntity tile = world.getTileEntity(x, y, z);
        float particleY;
        float particleX;
        float particleZ;
        if (side.offsetZ == 0)
            particleZ = z + side.offsetZ + rand.nextFloat();
        else
            particleZ = z + side.offsetZ + (side.offsetZ == -1 ? 0.99F : 0.01F);
        if (side.offsetX == 0)
            particleX = x + side.offsetX + rand.nextFloat();
        else
            particleX = x + side.offsetX + (side.offsetX == -1 ? 0.99F : 0.01F);
        if (side.offsetY == 0)
            particleY = y + side.offsetY + rand.nextFloat();
        else
            particleY = y + side.offsetY + (side.offsetY == -1 ? .968F : 0.01F);
        if (tile != null && tile instanceof TileEntityCustomColor)
            Minecraft.getMinecraft().effectRenderer.addEffect(new SparkleFX(world, particleX, particleY, particleZ, 0, 0, 0, ((TileEntityCustomColor) tile).getR(), ((TileEntityCustomColor) tile).getG(), ((TileEntityCustomColor) tile).getB()));
        else
            Minecraft.getMinecraft().effectRenderer.addEffect(new SparkleFX(world, particleX, particleY, particleZ, 0, 0, 0, world.getBlockMetadata(x, y, z)));
    }

    @Override
    public boolean canCreatureSpawn(EnumCreatureType type, IBlockAccess world, int x, int y, int z) {
        return false;
    }

    @Override
    public int damageDropped(int meta) {
        return meta;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void setGLColor(int meta) {
        MLColors.VALID_COLORS[meta].c.glColour();
    }

    @Override
    @SideOnly(Side.CLIENT)
    public int getRenderType() {
        return RenderGlow.modelId;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void setRGBColor(int meta) {
        Tessellator.instance.setColorRGBA_I(MLColors.VALID_COLORS[meta].rgb, 255);
    }

    @Override
    public int getBrightness(IBlockAccess world, int x, int y, int z) {
        return 15 << 20 | 15 << 4;
    }

    @Override
    public void getSubBlocks(Item item, CreativeTabs tab, List list) {
        for (final EnumStained s : EnumStained.VALID_TYPES)
            list.add(s.getBlockItemStack(this));
    }

    @Override
    @SideOnly(Side.CLIENT)
    public boolean isOpaqueCube() {
        return true;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void randomDisplayTick(World world, int x, int y, int z, Random rand) {
        for (ForgeDirection side : ForgeDirection.VALID_DIRECTIONS) {
            if (!(world.getBlock(x + side.offsetX, y + side.offsetY, z + side.offsetZ) instanceof BlockLus)) {
                renderParticlesAllSides(world, side, rand, x, y, z);
            } else if (world.getBlockMetadata(x + side.offsetX, y + side.offsetY, z + side.offsetZ) != world.getBlockMetadata(x, y, z)) {
                renderParticlesAllSides(world, side, rand, x, y, z);
            }
        }
    }

    @Override
    public void registerBlockIcons(IIconRegister reg) {
        baseTexture = reg.registerIcon(Reference.MOD_ID.toLowerCase() + ":overlay/stainedEffect");
        glassTexture = reg.registerIcon(Reference.MOD_ID.toLowerCase() + ":overlay/dyeGlassBase");
        particleIcon = reg.registerIcon(Reference.MOD_ID.toLowerCase() + ":sparkleFX");
        blockIcon = reg.registerIcon(Reference.MOD_ID.toLowerCase() + ":overlay/" + getTextureName());
    }

    @Override
    public int colorMultiplier(IBlockAccess world, int x, int y, int z) {
        if (hasColorMultiplier) {
            return MLColors.VALID_COLORS[world.getBlockMetadata(x, y, z)].c.rgb();
        }
        return super.colorMultiplier(world, x, y, z);
    }

    @Override
    public int getBrightness() {
        return 220;
    }

    public BlockLus setHasColorMultiplier(boolean hasColorMultiplier) {
        this.hasColorMultiplier = hasColorMultiplier;
        return this;
    }

    public boolean hasColorMultiplier(){
        return hasColorMultiplier;
    }

    @Override
    public boolean isNormalCube() {
        return true;
    }
}
