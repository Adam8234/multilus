package com.adam8234.multilus.blocks.lus.custom;

import com.adam8234.multilus.blocks.lus.IBlockLus;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.world.IBlockAccess;

public interface IBlockLusCustom extends IBlockLus {
    public void getRGBColor(IBlockAccess world, int x, int y, int z, Tessellator tess);
}
