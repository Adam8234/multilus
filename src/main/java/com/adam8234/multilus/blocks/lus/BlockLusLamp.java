package com.adam8234.multilus.blocks.lus;

import com.adam8234.multilus.network.PacketHandler;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

public class BlockLusLamp extends BlockLus {

    private final boolean defaultState;

    public BlockLusLamp(boolean defaultState) {
        super(Material.glass, "lusLamp");
        setBlockName("blockLusLamp_" + defaultState);
        this.defaultState = defaultState;
    }

    public static boolean isBlockIndirectlyGettingPowered(IBlockAccess iBlockAccess, int x, int y, int z) {
        for (ForgeDirection side : ForgeDirection.VALID_DIRECTIONS) {
            if (isBlockIndirectlyProvidingPowerTo(iBlockAccess, x + side.offsetX, y + side.offsetY, z + side.offsetZ, side)) {
                return true;
            }
            if ((side != ForgeDirection.DOWN) && (isRecieveingRedstonePower(iBlockAccess, x + side.offsetX, y + side.offsetY, z + side.offsetZ))) {
                return true;
            }
        }
        return isBlockGettingPowered(iBlockAccess, x, y, z);
    }

    public static boolean isRecieveingRedstonePower(IBlockAccess iBlockAccess, int x, int y, int z) {
        if (iBlockAccess.getBlock(x, y, z) == Blocks.redstone_wire) {
            int meta = iBlockAccess.getBlockMetadata(x, y, z);
            if (meta > 0) {
                return true;
            }
        }
        return false;
    }

    public static boolean isBlockIndirectlyProvidingPowerTo(IBlockAccess iBlockAccess, int x, int y, int z, ForgeDirection side) {
        if (iBlockAccess.isSideSolid(x, y, z, side, true)) {
            return isBlockGettingPowered(iBlockAccess, x, y, z);
        }
        Block block = iBlockAccess.getBlock(x, y, z);
        if (block != null) {
            return block.isProvidingWeakPower(iBlockAccess, x, y, z, side.ordinal()) > 0;
        }
        return false;
    }

    public static boolean isBlockGettingPowered(IBlockAccess iBlockAccess, int x, int y, int z) {
        for (ForgeDirection side : ForgeDirection.VALID_DIRECTIONS) {
            if (isBlockProvidingPowerTo(iBlockAccess, x + side.offsetX, y + side.offsetY, z + side.offsetZ, side)) {
                return true;
            }
            if ((side != ForgeDirection.DOWN) && (isRecieveingRedstonePower(iBlockAccess, x + side.offsetX, y + side.offsetY, z + side.offsetZ))) {
                return true;
            }
            if(iBlockAccess.getBlock(x + side.offsetX, y + side.offsetY, z + side.offsetZ) == Blocks.redstone_block){
                return true;
            }
        }
        return false;
    }

    public static boolean isBlockProvidingPowerTo(IBlockAccess iBlockAccess, int x, int y, int z, ForgeDirection side) {
        Block block = iBlockAccess.getBlock(x, y, z);
        if (block != null) {
            return block.isProvidingStrongPower(iBlockAccess, x, y, z, side.ordinal()) == 15;
        }
        return false;
    }

    @Override
    public int getLightValue(IBlockAccess world, int x, int y, int z) {
        return getToggle(world, x, y, z) ? 15 : super.getLightValue(world, x, y, z);
    }

    private boolean getToggle(IBlockAccess world, int x, int y, int z) {
        return defaultState ? !isBlockIndirectlyGettingPowered(world, x, y, z) : isBlockIndirectlyGettingPowered(world, x, y, z);
    }

    @Override
    public void onNeighborChange(IBlockAccess world, int x, int y, int z, int tileX, int tileY, int tileZ) {
        getLightValue(world, x, y, z);
        super.onNeighborChange(world, x, y, z, tileX, tileY, tileZ);
    }

    @Override
    public void onNeighborBlockChange(World world, int x, int y, int z, Block block) {
        world.markBlockForUpdate(x, y, z);
        super.onNeighborBlockChange(world, x, y, z, block);
    }

    @Override
    public boolean canConnectRedstone(IBlockAccess world, int x, int y, int z, int side) {
        return true;
    }

    @Override
    public boolean canProvidePower() {
        return true;
    }

    @Override
    public int getBrightness(IBlockAccess world, int x, int y, int z) {
        return super.getBrightness(world, x, y, z);
    }

    @Override
    public int getBrightness() {
        return defaultState ? 220 : 75;
    }
}
