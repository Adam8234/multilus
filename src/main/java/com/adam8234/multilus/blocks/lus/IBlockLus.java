package com.adam8234.multilus.blocks.lus;

import net.minecraft.client.renderer.Tessellator;
import net.minecraft.world.IBlockAccess;

public interface IBlockLus {
    public void setGLColor(int meta);

    void setRGBColor(int meta);

    public int getBrightness(IBlockAccess world, int x, int y, int z);

    public int getBrightness();
}
