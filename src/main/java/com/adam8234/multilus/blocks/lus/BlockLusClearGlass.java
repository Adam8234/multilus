package com.adam8234.multilus.blocks.lus;

import com.adam8234.multilus.MultiLus;
import com.adam8234.multilus.common.MLBlocks;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;

public class BlockLusClearGlass extends Block {
    public BlockLusClearGlass() {
        super(Material.glass);
        setHardness(0.3F);
        setResistance(1.5F);
        setCreativeTab(MultiLus.tabBlock);
        setBlockName("blockClearGlass");
    }

    @Override
    public IIcon getIcon(IBlockAccess world, int x, int y, int z, int side) {
        return ((BlockLusConnected) MLBlocks.LUS_STORAGE.block).getConnectedBlockTexture(world, x, y, z, side, this);
    }

    @Override
    public IIcon getIcon(int side, int meta) {
        return ((BlockLusConnected) MLBlocks.LUS_STORAGE.block).getIcon(side, meta);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockAccess world, int x, int y, int z, int side) {
        final Block block = world.getBlock(x, y, z);
        return block != this && super.shouldSideBeRendered(world, x, y, z, side);
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }
}
