package com.adam8234.multilus.blocks.mech;

import com.adam8234.multilus.MultiLus;
import com.adam8234.multilus.handler.GuiHandler;
import com.adam8234.multilus.item.lus.tool.ItemLusInterface;
import com.adam8234.multilus.tile.TileEntityCrystalizer;
import com.adam8234.multilus.tile.TileEntityCustomColor;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockCrystalizer extends Block implements ITileEntityProvider{
    public BlockCrystalizer() {
        super(Material.rock);
        setCreativeTab(MultiLus.tabBlock);
        setBlockName("blockCrystalizer");
    }

    @Override
    public TileEntity createNewTileEntity(World world, int meta) {
        return new TileEntityCrystalizer();
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9) {
        if (player.getHeldItem() != null) {
            return player.getHeldItem().getItem() instanceof ItemLusInterface;
        }
        if (!player.isSneaking()) {
            if (!world.isRemote && world.getTileEntity(x, y, z) instanceof TileEntityCrystalizer) {
                player.openGui(MultiLus.instance, GuiHandler.GuiIds.CRYSTALIZER.id, world, x, y, z);
            }
            return true;
        }
        return true;
    }
}
