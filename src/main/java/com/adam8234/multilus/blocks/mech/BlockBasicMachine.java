package com.adam8234.multilus.blocks.mech;

import com.adam8234.multilus.MultiLus;
import com.adam8234.multilus.blocks.lus.BlockLus;
import com.adam8234.multilus.reference.Reference;
import com.adam8234.multilus.tile.TileEntityBasicMachine;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

import java.util.List;

public class BlockBasicMachine extends BlockLus implements ITileEntityProvider {

    private static IIcon iceIcon;
    private static IIcon lavaIcon;
    private static IIcon waterIcon;

    public BlockBasicMachine() {
        super(Material.rock);
        this.setHardness(2.0F);
        this.setCreativeTab(MultiLus.tabBlock);
        this.setBlockName("blockBasicMachine");
        this.setBlockTextureName("basicMachineTop");
    }

    private void checkForHarden(World w, int x, int y, int z) {
        ForgeDirection lava = null;
        for (final ForgeDirection side : ForgeDirection.values())

            if (w.getBlock(x - side.offsetX, y + side.offsetY, z + side.offsetZ) == Block.getBlockFromName("lava")
                    || w.getBlock(x + side.offsetX, y + side.offsetY, z + side.offsetZ) == Block.getBlockFromName("flowing_lava")) {
                lava = side;
                break;
            }
        if (lava != null) {
            final int meta = w.getBlockMetadata(x + lava.offsetX, y + lava.offsetY, z + lava.offsetZ);
            switch (meta) {
                case 0:
                    w.setBlock(x + lava.offsetX, y + lava.offsetY, z + lava.offsetZ, Blocks.obsidian);
                    break;
                default:
                    w.setBlock(x + lava.offsetX, y + lava.offsetY, z + lava.offsetZ, Blocks.cobblestone);
            }
            triggerLavaMixEffects(w, x + lava.offsetX, y + lava.offsetY, z + lava.offsetZ);
        }
    }

    private void checkForIce(World w, int x, int y, int z) {
        ForgeDirection water = null;
        for (final ForgeDirection side : ForgeDirection.values())
            if (w.getBlock(x + side.offsetX, y + side.offsetY, z + side.offsetZ) == Blocks.water || w.getBlock(x + side.offsetX, y + side.offsetY, z + side.offsetZ) == Blocks.flowing_water) {
                water = side;
                break;
            }
        if (water != null)
            w.setBlock(x + water.offsetX, y + water.offsetY, z + water.offsetZ, Blocks.ice);
    }

    private void checkForWater(World w, int x, int y, int z) {
        ForgeDirection water = null;
        for (final ForgeDirection side : ForgeDirection.values())
            if (w.getBlock(x + side.offsetX, y + side.offsetY, z + side.offsetZ) == Blocks.water || w.getBlock(x + side.offsetX, y + side.offsetY, z + side.offsetZ) == Blocks.flowing_water) {
                water = side;
                break;
            }
        if (water != null) {
            w.setBlock(x + water.offsetX, y + water.offsetY, z + water.offsetZ, Blocks.stone);
            triggerLavaMixEffects(w, x + water.offsetX, y + water.offsetY, z + water.offsetZ);
        }
    }

    @Override
    public IIcon getIcon(int side, int meta) {
        switch (ForgeDirection.VALID_DIRECTIONS[side]) {
            case DOWN:
                return blockIcon;
            case UP:
                return blockIcon;
            default:
                switch (meta) {
                    case 14:
                        return lavaIcon;
                    case 11:
                        return waterIcon;
                    default:
                        return blockIcon;
                }
        }
    }

    @Override
    public void getSubBlocks(Item item, CreativeTabs tab, List list) {
        list.add(new ItemStack(this, 1, 14));
        list.add(new ItemStack(this, 1, 11));
    }

    @Override
    @SideOnly(Side.CLIENT)
    public boolean isOpaqueCube() {
        return true;
    }

    @Override
    public int getLightValue(IBlockAccess world, int x, int y, int z) {
        TileEntityBasicMachine tileWorld = (TileEntityBasicMachine) world.getTileEntity(x, y, z);
        if (tileWorld != null) {
            return tileWorld.hasCobbleGen ? 15 : super.getLightValue(world, x, y, z);
        }
        return 15;
    }

    @Override
    public int onBlockPlaced(World w, int x, int y, int z, int par5, float par6, float par7, float par8, int meta) {
        switch (meta) {
            case 14:
                for (int i = 0; i < 6; i++)
                    checkForWater(w, x, y, z);
                break;
            case 11:
                for (int i = 0; i < 6; i++)
                    checkForHarden(w, x, y, z);
                break;
        }
        return meta;
    }

    @Override
    public void onNeighborBlockChange(World w, int x, int y, int z, Block block) {
        if (w.getBlock(x, y, z) == this)
            switch (w.getBlockMetadata(x, y, z)) {
                case 14:
                    checkForWater(w, x, y, z);
                    break;
                case 11:
                    checkForHarden(w, x, y, z);
            }
    }

    @Override
    public void registerBlockIcons(IIconRegister reg) {
        waterIcon = reg.registerIcon(Reference.MOD_ID.toLowerCase() + ":overlay/blockWater3");
        lavaIcon = reg.registerIcon(Reference.MOD_ID.toLowerCase() + ":overlay/blockFire3");
        super.registerBlockIcons(reg);
    }

    private void triggerLavaMixEffects(World w, int x, int y, int z) {
        w.playSoundEffect(x + 0.5F, y + 0.5F, z + 0.5F, "random.fizz", 0.5F, 2.6F + (w.rand.nextFloat() - w.rand.nextFloat()) * 0.8F);

        for (int l = 0; l < 8; ++l) {
            w.spawnParticle("largesmoke", x + Math.random(), y + 1.2D, z + Math.random(), 0.0D, 0.0D, 0.0D);
        }
    }

    @Override
    public TileEntity createNewTileEntity(World world, int meta) {
        return new TileEntityBasicMachine(meta);
    }


}
