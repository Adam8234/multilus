package com.adam8234.multilus.blocks;

import com.adam8234.multilus.MultiLus;
import com.adam8234.multilus.blocks.BlockSpecialStone.EnumSpecialStone;
import com.adam8234.multilus.common.MLBlocks;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockWall;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

import java.util.List;

public class BlockSpecialStoneWall extends BlockWall {

    public BlockSpecialStoneWall() {
        super(Block.getBlockFromName("stone"));
        setBlockName("blockObfuscousStoneWall");
        setCreativeTab(MultiLus.tabBlock);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int meta) {
        return EnumSpecialStone.VALID_STONE[meta].texture;
    }

    @Override
    public void getSubBlocks(Item item, CreativeTabs tab, List list) {
        for (final EnumSpecialStone s : EnumSpecialStone.VALID_STONE)
            list.add(new ItemStack(MLBlocks.SPECIAL_STONE_WALL.block, 1, s.meta));
    }

}
