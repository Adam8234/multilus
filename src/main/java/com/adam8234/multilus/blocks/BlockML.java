package com.adam8234.multilus.blocks;

import com.adam8234.multilus.MultiLus;
import com.adam8234.multilus.reference.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockML extends Block {
    public BlockML(Material material) {
        super(material);
        this.setCreativeTab(MultiLus.tabBlock);
    }

    public BlockML() {
        this(Material.rock);
    }

    @Override
    public String getUnlocalizedName() {
        return String.format("tile.%s", getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
    }

    protected String getUnwrappedUnlocalizedName(String unlocalizedName) {
        return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
    }
}
