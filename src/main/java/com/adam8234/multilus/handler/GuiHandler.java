package com.adam8234.multilus.handler;

import com.adam8234.multilus.client.gui.GuiCrystalizer;
import com.adam8234.multilus.client.gui.GuiRGB;
import com.adam8234.multilus.inventory.ContainerCrystalizer;
import com.adam8234.multilus.inventory.ContainerEmpty;
import com.adam8234.multilus.inventory.ContainerRGB;
import com.adam8234.multilus.tile.TileEntityCrystalizer;
import com.adam8234.multilus.tile.TileEntityCustomColor;
import cpw.mods.fml.common.network.IGuiHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class GuiHandler implements IGuiHandler {
    @Override
    public Object getServerGuiElement(int id, EntityPlayer player, World world, int x, int y, int z) {
        switch (GuiIds.VALID_TYPES[id]) {
            case RGB:
                TileEntityCustomColor tileEntityCustomColor = (TileEntityCustomColor) world.getTileEntity(x, y, z);
                return new ContainerRGB(player.inventory, tileEntityCustomColor);
            case CRYSTALIZER:
                TileEntityCrystalizer tileEntityCrystalizer = (TileEntityCrystalizer) world.getTileEntity(x, y, z);
                return new ContainerCrystalizer(player.inventory, tileEntityCrystalizer);
        }
        return null;
    }

    @Override
    public Object getClientGuiElement(int id, EntityPlayer player, World world, int x, int y, int z) {
        switch (GuiIds.VALID_TYPES[id]) {
            case RGB:
                TileEntityCustomColor tileEntityCustomColor = (TileEntityCustomColor) world.getTileEntity(x, y, z);
                return new GuiRGB(player.inventory, tileEntityCustomColor);
            case CRYSTALIZER:
                TileEntityCrystalizer tileEntityCrystalizer = (TileEntityCrystalizer) world.getTileEntity(x, y, z);
                return new GuiCrystalizer(player.inventory, tileEntityCrystalizer);

        }
        return null;
    }

    public enum GuiIds {
        RGB,
        CRYSTALIZER;
        public static final GuiIds[] VALID_TYPES = values();
        public final int id = ordinal();
    }
}
