package com.adam8234.multilus.handler;

import com.adam8234.multilus.blocks.lus.BlockLus;
import com.adam8234.multilus.item.lus.tool.ItemLusInterface;
import com.adam8234.multilus.tile.TileEntityCustomColor;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;

public class MultiLusEventHandler {

    @SubscribeEvent
    public void onPlayerRightClick(PlayerInteractEvent e) {
        if (e.action == PlayerInteractEvent.Action.RIGHT_CLICK_BLOCK)
            onPlayerRightClickBlock(e);
    }

    private void onPlayerRightClickBlock(PlayerInteractEvent e) {
        final EntityPlayer player = e.entityPlayer;
        final World world = player.getEntityWorld();
        final Block block = world.getBlock(e.x, e.y, e.z);
        if(!(block instanceof BlockLus)){
            return;
        }
        final TileEntity tile = world.getTileEntity(e.x, e.y, e.z);
        if (tile != null && tile instanceof TileEntityCustomColor) {
            if (player.getHeldItem() != null) {
                final ItemStack itemStack = player.getHeldItem();
                if (itemStack.getItem() != null && itemStack.getItem() instanceof ItemLusInterface) {
                    if (player.isSneaking()) {
                        if (itemStack.stackTagCompound == null) {
                            itemStack.stackTagCompound = new NBTTagCompound();
                        }
                        itemStack.stackTagCompound.setInteger("r", ((TileEntityCustomColor) tile).getR());
                        itemStack.stackTagCompound.setInteger("g", ((TileEntityCustomColor) tile).getG());
                        itemStack.stackTagCompound.setInteger("b", ((TileEntityCustomColor) tile).getB());
                    } else {
                        if (itemStack.stackTagCompound == null) {
                            itemStack.stackTagCompound = new NBTTagCompound();
                        }
                        ((TileEntityCustomColor) tile).setR(itemStack.stackTagCompound.getInteger("r"));
                        ((TileEntityCustomColor) tile).setG(itemStack.stackTagCompound.getInteger("g"));
                        ((TileEntityCustomColor) tile).setB(itemStack.stackTagCompound.getInteger("b"));
                        ((TileEntityCustomColor) tile).updateBlock();
                    }
                }
            }
        }

    }
}
