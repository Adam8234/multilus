package com.adam8234.multilus.item.lus;

import codechicken.lib.colour.ColourRGBA;
import com.adam8234.multilus.reference.MLColors;
import com.adam8234.multilus.reference.Reference;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

public abstract class ItemLus extends Item implements IItemLus{
    public static IIcon animationIcon;

    @Override
    public ColourRGBA getColour(ItemStack itemStack) {
        return new ColourRGBA(MLColors.VALID_COLORS[itemStack.getItemDamage()].c.rgb());
    }

    @Override
    public void registerIcons(IIconRegister reg) {
        animationIcon = reg.registerIcon(Reference.MOD_ID.toLowerCase() + ":stainedEffect");
    }

    @Override
    public int getColorFromItemStack(ItemStack par1ItemStack, int par2) {
        return MLColors.get(par1ItemStack.getItemDamage()).rgb;
    }
}
