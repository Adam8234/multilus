package com.adam8234.multilus.item.lus.tool;

import com.adam8234.multilus.MultiLus;
import com.adam8234.multilus.reference.MLColors;
import com.adam8234.multilus.reference.Reference;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;

public class ItemLusPickaxe extends ItemPickaxe {

    private final MLColors e;

    public ItemLusPickaxe(MLColors e) {
        super(MultiLus.lusToolMaterial);
        setUnlocalizedName("itemLusPickaxe." + e.meta);
        setCreativeTab(MultiLus.tabItem);
        this.e = e;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public int getColorFromItemStack(ItemStack par1ItemStack, int par2) {
        return e.rgb;
    }

    @Override
    public void registerIcons(IIconRegister par1IconRegister) {
        itemIcon = par1IconRegister.registerIcon(Reference.MOD_ID.toLowerCase() + ":lusPickaxe");
    }

}
