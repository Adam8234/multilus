package com.adam8234.multilus.item.lus;

import codechicken.lib.colour.ColourRGBA;
import com.adam8234.multilus.MultiLus;
import com.adam8234.multilus.reference.EnumStained;
import com.adam8234.multilus.reference.MLColors;
import com.adam8234.multilus.reference.Reference;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

import java.util.List;

public class ItemLusCrystal extends ItemLus{

    public ItemLusCrystal() {
        setUnlocalizedName("itemLusCrystal");
        setCreativeTab(MultiLus.tabItem);
        setHasSubtypes(true);
    }

    @Override
    public void getSubItems(Item item, CreativeTabs tabs, List list) {
        for (final EnumStained e : EnumStained.VALID_TYPES)
            list.add(e.getCrystal());
    }

    @Override
    public void registerIcons(IIconRegister reg) {
        itemIcon = reg.registerIcon(Reference.MOD_ID.toLowerCase() + ":dyeCrystal");
        super.registerIcons(reg);
    }

    @Override
    public ColourRGBA getColour(ItemStack itemStack) {
        return new ColourRGBA(MLColors.VALID_COLORS[itemStack.getItemDamage()].c.rgba());
    }

    @Override
    public IIcon getMaskIcon() {
        return null;
    }
}
