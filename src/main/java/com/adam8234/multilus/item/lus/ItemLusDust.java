package com.adam8234.multilus.item.lus;

import com.adam8234.multilus.MultiLus;
import com.adam8234.multilus.reference.EnumStained;
import com.adam8234.multilus.reference.MLColors;
import com.adam8234.multilus.reference.Reference;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.List;

public class ItemLusDust extends Item {

    public ItemLusDust() {
        super();
        setCreativeTab(MultiLus.tabItem);
        setUnlocalizedName("itemLusDust");
        setHasSubtypes(true);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public int getColorFromItemStack(ItemStack par1ItemStack, int par2) {
        return MLColors.get(par1ItemStack.getItemDamage()).rgb;
    }

    @Override
    public void getSubItems(Item items, CreativeTabs tab, List list) {
        for (final EnumStained s : EnumStained.VALID_TYPES)
            list.add(s.getDust());
    }

    @Override
    public String getUnlocalizedName(ItemStack itemstack) {
        return getUnlocalizedName();
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister reg) {
        itemIcon = reg.registerIcon(Reference.MOD_ID.toLowerCase() + ":dyeDust");
    }

}
