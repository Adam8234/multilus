/*
package com.adam8234.multilus.item.lus.tool;

import buildcraft.api.tools.IToolWrench;
import com.adam8234.multilus.MultiLus;
import com.adam8234.multilus.blocks.lus.BlockLus;
import com.adam8234.multilus.blocks.lus.BlockLusOre;
import com.adam8234.multilus.blocks.lus.custom.IBlockLusCustom;
import com.adam8234.multilus.client.particles.SparkleFX;
import com.adam8234.multilus.reference.EnumStained;
import com.adam8234.multilus.reference.MLColors;
import com.adam8234.multilus.reference.Reference;
import com.adam8234.multilus.tile.TileEntityCustomColor;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class ItemLusWrench extends Item implements IToolWrench {

    private final Set<Class<? extends Block>> shiftRotations = new HashSet<Class<? extends Block>>();

    public ItemLusWrench() {
        super();
        setUnlocalizedName("itemLusWrench");
        setMaxStackSize(1);
        setCreativeTab(MultiLus.tabItem);
        setHasSubtypes(true);
    }

    @Override
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par2List, boolean par4) {
        par2List.add("\u00a7a" + "Remove Glow Blocks With Ease");
        par2List.add("Shift Right Click Removes Block");
    }

    @Override
    public boolean canWrench(EntityPlayer player, int x, int y, int z) {
        return true;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public int getColorFromItemStack(ItemStack par1ItemStack, int par2) {
        return MLColors.get(par1ItemStack.getItemDamage()).rgb;
    }

    @Override
    public void getSubItems(Item item, CreativeTabs par2CreativeTabs, List list) {
        for (final EnumStained s : EnumStained.VALID_TYPES)
            list.add(new ItemStack(this, 1, s.meta));
    }

    @Override
    public String getUnlocalizedName(ItemStack itemstack) {
        return getUnlocalizedName();
    }

    private boolean isShiftRotation(Class<? extends Block> cls) {
        for (final Class<? extends Block> shift : shiftRotations)
            if (shift.isAssignableFrom(cls))
                return true;
        return false;
    }

    @Override
    public boolean onItemUseFirst(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) {
        final int meta = world.getBlockMetadata(x, y, z);
        final Block block = world.getBlock(x, y, z);
        TileEntityCustomColor tile;
        if (block instanceof IBlockLusCustom)
            tile = (TileEntityCustomColor) world.getTileEntity(x, y, z);
        else
            tile = null;
        player.swingItem();
        if (player.isSneaking() && block instanceof BlockLus && !(block instanceof BlockLusOre)) {
            final Random rand = new Random();
            player.swingItem();
            for (int i = 0; i < 30; i++) {
                final float particleX = x + rand.nextFloat();
                final float particleY = y + rand.nextFloat();
                final float particleZ = z + rand.nextFloat();
                if (tile != null)
                    Minecraft.getMinecraft().effectRenderer.addEffect(new SparkleFX(world, particleX, particleY, particleZ, 0, 0, 0, tile.r, tile.g, tile.b));
                else
                    Minecraft.getMinecraft().effectRenderer.addEffect(new SparkleFX(world, particleX, particleY, particleZ, 0, 0, 0, world.getBlockMetadata(x, y, z)));
            }
            world.setBlockToAir(x, y, z);
            block.harvestBlock(world, player, x, y, z, meta);
        }

        if (player.isSneaking() != isShiftRotation(block.getClass()))
            return false;

        if (block.rotateBlock(world, x, y, z, ForgeDirection.getOrientation(side))) {

            return !world.isRemote;
        }
        return false;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister reg) {
        itemIcon = reg.registerIcon(Reference.ID.toLowerCase() + ":egWrenchV2");
    }

    @Override
    public void wrenchUsed(EntityPlayer player, int x, int y, int z) {
        player.swingItem();
    }
}
*/