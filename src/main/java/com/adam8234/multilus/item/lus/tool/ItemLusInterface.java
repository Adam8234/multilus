package com.adam8234.multilus.item.lus.tool;

import codechicken.lib.colour.ColourRGBA;
import com.adam8234.multilus.MultiLus;
import com.adam8234.multilus.item.lus.IItemLus;
import com.adam8234.multilus.item.lus.ItemLus;
import com.adam8234.multilus.reference.MLColors;
import com.adam8234.multilus.reference.Reference;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraftforge.client.IItemRenderer;

import java.awt.*;
import java.util.List;

public class ItemLusInterface extends ItemLus{
    private IIcon maskIcon;

    public ItemLusInterface() {
        super();
        setUnlocalizedName("itemLusInterface");
        setCreativeTab(MultiLus.tabItem);
        setMaxStackSize(1);
    }

    @Override
    public void addInformation(ItemStack itemStack, EntityPlayer player, List list, boolean par4) {
        list.add("Shift right click to copy a color");
        list.add("Right click to paste a color");
        if (itemStack.stackTagCompound != null) {
            final int r = itemStack.stackTagCompound.getInteger("r");
            final int g = itemStack.stackTagCompound.getInteger("g");
            final int b = itemStack.stackTagCompound.getInteger("b");
            list.add(EnumChatFormatting.RED + "Red: " + r);
            list.add(EnumChatFormatting.GREEN + "Green: " + g);
            list.add(EnumChatFormatting.BLUE + "Blue: " + b);
        }
    }

    @Override
    public int getColorFromItemStack(ItemStack itemStack, int par2) {
        if (itemStack.getTagCompound() != null) {
            final int r = itemStack.stackTagCompound.getInteger("r");
            final int g = itemStack.stackTagCompound.getInteger("g");
            final int b = itemStack.stackTagCompound.getInteger("b");
            return new ColourRGBA(r, g, b, 255).rgb();
        } else
            return MLColors.WHITE.rgb;
    }

    @Override
    public void onCreated(ItemStack itemStack, World world, EntityPlayer player) {
        itemStack.stackTagCompound = new NBTTagCompound();
        itemStack.stackTagCompound.setInteger("r", 256);
        itemStack.stackTagCompound.setInteger("g", 256);
        itemStack.stackTagCompound.setInteger("b", 256);
    }

    @Override
    public void registerIcons(IIconRegister reg) {
        itemIcon = reg.registerIcon(Reference.MOD_ID.toLowerCase() + ":colorCopier");
        maskIcon  = reg.registerIcon(Reference.MOD_ID.toLowerCase() + ":colorCopier_mask");
        super.registerIcons(reg);
    }

    @Override
    public IIcon getIcon(ItemStack stack, int pass) {
        return pass == 1 ? maskIcon : itemIcon;
    }

    @Override
    public ColourRGBA getColour(ItemStack itemStack) {
        if(itemStack.stackTagCompound != null){
            int r = itemStack.stackTagCompound.getInteger("r");
            int g = itemStack.stackTagCompound.getInteger("g");
            int b = itemStack.stackTagCompound.getInteger("b");
            return new ColourRGBA(r, g, b, 255);
        }
        return new ColourRGBA(255, 255, 255, 255);
    }

    @Override
    public IIcon getMaskIcon() {
        return maskIcon;
    }
}
