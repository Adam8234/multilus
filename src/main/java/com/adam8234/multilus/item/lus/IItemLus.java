package com.adam8234.multilus.item.lus;

import codechicken.lib.colour.ColourRGBA;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

public interface IItemLus {
    public ColourRGBA getColour(ItemStack itemStack);
    public IIcon getMaskIcon();
}
