package com.adam8234.multilus.item;

import com.adam8234.multilus.MultiLus;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

import java.util.List;

public class ItemStaff extends Item {

    public ItemStaff() {
        super();
        setUnlocalizedName("itemStaff");
        setCreativeTab(MultiLus.tabItem);
        setMaxStackSize(1);
    }

    @Override
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
        switch (par1ItemStack.getItemDamage()) {
            case 0:
                par3List.add(EnumChatFormatting.RED + "WIP");
                par3List.add("Right Click A Entity (Shoots it up)");
                break;
            case 1:
                par3List.add(EnumChatFormatting.RED + "WIP");
                par3List.add("Right Click A Entity (Set's On Fire)");
                break;
            case 2:
                par3List.add(EnumChatFormatting.RED + "WIP");
                par3List.add("Right Click Crops (BoneMeal)");
                break;

            default:
                break;
        }
    }

    @Override
    public void getSubItems(Item item, CreativeTabs tab, List list) {
        for (int i = 0; i < 3; i++)
            list.add(new ItemStack(this, 1, i));
    }

    @Override
    public String getUnlocalizedName(ItemStack itemstack) {
        return getUnlocalizedName() + ".staff" + itemstack.getItemDamage();
    }

    @Override
    public boolean itemInteractionForEntity(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, EntityLivingBase par3EntityLivingBase) {
        switch (par1ItemStack.getItemDamage()) {
            case 0:
                par3EntityLivingBase.setVelocity(0.0f, 2f, 0.0f);
                break;
            case 1:
                par3EntityLivingBase.setFire(5);
                break;

            default:
                break;
        }
        par2EntityPlayer.swingItem();
        return true;
    }

    @Override
    public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10) {
        if (par1ItemStack.getItemDamage() == 2) {
            par2EntityPlayer.swingItem();
            if (ItemDye.applyBonemeal(par1ItemStack, par3World, par4, par5, par6, par2EntityPlayer))
                par3World.playAuxSFX(2005, par4, par5, par6, 0);
            return true;
        }
        return false;
    }
}
