package com.adam8234.multilus.inventory;

import com.adam8234.multilus.tile.TileEntityCrystalizer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerCrystalizer extends Container {
    private final int PLAYER_INVENTORY_ROWS = 3;
    private final int PLAYER_INVENTORY_COLUMNS = 9;
    private final InventoryPlayer inventoryPlayer;
    private final TileEntityCrystalizer tileEntityCrystalizer;

    public ContainerCrystalizer(InventoryPlayer inventoryPlayer, TileEntityCrystalizer tileEntityCrystalizer) {
        this.inventoryPlayer = inventoryPlayer;
        this.addSlotToContainer(new Slot(tileEntityCrystalizer, 0, 80, 22));
        // Add the player's inventory slots to the container
        for (int inventoryRowIndex = 0; inventoryRowIndex < PLAYER_INVENTORY_ROWS; ++inventoryRowIndex) {
            for (int inventoryColumnIndex = 0; inventoryColumnIndex < PLAYER_INVENTORY_COLUMNS; ++inventoryColumnIndex) {
                this.addSlotToContainer(new Slot(inventoryPlayer, inventoryColumnIndex + inventoryRowIndex * 9 + 9, 8 + inventoryColumnIndex * 18, 106 + inventoryRowIndex * 18 - 22));
            }
        }

        // Add the player's action bar slots to the container
        for (int actionBarSlotIndex = 0; actionBarSlotIndex < PLAYER_INVENTORY_COLUMNS; ++actionBarSlotIndex) {
            this.addSlotToContainer(new Slot(inventoryPlayer, actionBarSlotIndex, 8 + actionBarSlotIndex * 18, 142));
        }
        this.tileEntityCrystalizer = tileEntityCrystalizer;
    }

    @Override
    public boolean canInteractWith(EntityPlayer player) {
        return true;
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer entityPlayer, int slotIndex) {
        ItemStack itemStack = null;
        Slot slot = (Slot) inventorySlots.get(slotIndex);

        if (slot != null && slot.getHasStack()) {
            ItemStack slotItemStack = slot.getStack();
            itemStack = slotItemStack.copy();

            if (slotIndex < tileEntityCrystalizer.getSizeInventory()) {

                if (!this.mergeItemStack(slotItemStack, 1, inventorySlots.size(), true)) {
                    return null;
                }
            } else {
                if (!this.mergeItemStack(slotItemStack, 0, tileEntityCrystalizer.getSizeInventory(), false)) {
                    return null;
                }
            }

            if (slotItemStack.stackSize == 0) {
                slot.putStack(null);
            } else {
                slot.onSlotChanged();
            }
        }

        return itemStack;
    }

}
