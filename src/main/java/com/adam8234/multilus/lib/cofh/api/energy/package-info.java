/**
 * (C) 2014 Team CoFH / CoFH / Cult of the Full Hub
 * http://www.teamcofh.com
 */
@API(apiVersion = "1.7.2R1.0.0", owner = "CoFHAPI", provides = "CoFHAPI|energy")
package com.adam8234.multilus.lib.cofh.api.energy;

import cpw.mods.fml.common.API;

